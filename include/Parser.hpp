#ifndef PARSER_HPP
#define PARSER_HPP

#include <McDeval.hpp>

#include <string>
#include <vector>

#include "Configuration.hpp"

namespace Parser {
    /** Creates a application configuration object by making sense of characters
     * loaded from the application configuration file. */
    McDeval::Configuration
    load_application_configuration(const std::string& app_config_filename);

    /** Creates a algorithm configuration object by making sense of characters
     * loaded from the algorithm configuration file. */
    AlgorithmConfiguration
    load_algorithm_configuration(const std::string& alg_config_filename);
}


#endif
