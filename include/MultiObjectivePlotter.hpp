#ifndef MULTIOBJECTIVEPLOTTER_HPP
#define MULTIOBJECTIVEPLOTTER_HPP

#include <array>
#include <memory>
#include <vector>

#include "Gnuplot.hpp"

class Plotter2D {
  private:
    std::unique_ptr<Gnuplot> gp;

  public:
    Plotter2D(std::array<double, 2> x_range, std::array<double, 2> y_range);

    void plot(const std::vector<std::pair<double, double>>& points);
    void plot(const std::vector<std::array<double, 2>>& points);
    void plot(const std::vector<std::vector<double>>& points);
};

#endif
