#ifndef ALGORITHM_ALGORITHM_HPP
#define ALGORITHM_ALGORITHM_HPP

#include <McDeval.hpp>

#include "Configuration.hpp"

class BaseAlgorithm {
  protected:
    const AlgorithmConfiguration& alg_config;
    const McDeval::Configuration& app_config;

  public:
    BaseAlgorithm(const AlgorithmConfiguration& alg_config,
                  const McDeval::Configuration& app_config);
    BaseAlgorithm(const BaseAlgorithm& other) = default;
    BaseAlgorithm(BaseAlgorithm&& other)      = delete;
    virtual ~BaseAlgorithm()                  = default;
    BaseAlgorithm& operator=(BaseAlgorithm&& other) = delete;
    BaseAlgorithm& operator=(const BaseAlgorithm& other) = delete;

  protected:
    /** Throws a runtime error with message describing which parameter is
     * missing and which algorithm is being used. */
    [[noreturn]] void
    throw_missing_parameter_error(const std::string& parameter_name) const;

  private:
    /** Makes sure that parameters required by all algorithms are set within
     * the algorithm configuration object. If a parameter is missing, runtime
     * error is thrown. */
    void validate_parameters() const;
};

class SingleObjectiveAlgorithm : public BaseAlgorithm {
  public:
    SingleObjectiveAlgorithm(const AlgorithmConfiguration& alg_config,
                             const McDeval::Configuration& app_config);

    /** Runs the optimization algorithm and returns the best solution. */
    virtual McDeval::Solution run() = 0;

    /** Returns risk of the best solution returned by the last algorithm run. */
    virtual McDeval::Risk get_best_solution_risk() const = 0;

    /** Creates instance of single objective algorithm based on the type (name)
     * given in the algorithm configuration. If single objective algorithm of
     * provided type (name) does not exist, nullptr is returned. */
    static std::unique_ptr<SingleObjectiveAlgorithm>
    create(const AlgorithmConfiguration& alg_config,
           const McDeval::Configuration& app_config);

  private:
    /** Makes sure that parameters required by all single objective algorithms
     * are set within the algorithm configuration object. If a parameter is
     * missing, runtime error is thrown. */
    void validate_parameters() const;
};

class MultiObjectiveAlgorithm : public BaseAlgorithm {
  public:
    MultiObjectiveAlgorithm(const AlgorithmConfiguration& alg_config,
                            const McDeval::Configuration& app_config);

    /** Runs the algorithm and returns nondominated solutions when the algorithm
     * is finished. Nondominated solutions are solutions at pareto frontier. */
    virtual std::vector<McDeval::Solution> run() = 0;

    /** Returns risks of solutions returned by the run function. */
    virtual std::vector<std::vector<McDeval::Risk>>
    get_nondominated_solution_risks() const = 0;

    /** Creates instance of multi objective algorithm based on the type (name)
     * given in the algorithm configuration. If multi objective algorithm of
     * provided type (name) does not exist, nullptr is returned. */
    static std::unique_ptr<MultiObjectiveAlgorithm>
    create(const AlgorithmConfiguration& alg_config,
           const McDeval::Configuration& app_config);

  private:
    /** Makes sure that parameters required by all multi objective algorithms
     * are set within the algorithm configuration object. If a parameter is
     * missing, runtime error is thrown. */
    void validate_parameters() const;
};

#endif
