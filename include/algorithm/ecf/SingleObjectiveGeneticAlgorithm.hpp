#ifndef ALGORITHM_ECF_SINGLEOBJECTIVEGENETICALGORITHM_HPP
#define ALGORITHM_ECF_SINGLEOBJECTIVEGENETICALGORITHM_HPP

#include <ECF.h>

#include "algorithm/Algorithm.hpp"

namespace ECF {

class SingleObjectiveGeneticAlgorithm : public SingleObjectiveAlgorithm {
  private:
    StateP state;

  public:
    SingleObjectiveGeneticAlgorithm(const AlgorithmConfiguration& alg_config,
                                    const McDeval::Configuration& app_config);

    McDeval::Solution run() override;

    McDeval::Risk get_best_solution_risk() const override;
};

}

#endif
