#ifndef ALGORITHM_ECF_CONFIGURATIONFILE_HPP
#define ALGORITHM_ECF_CONFIGURATIONFILE_HPP

class AlgorithmConfiguration;

namespace ECF {

/** Converts algorithm configuration object into ECF configuration file. This is
 * required since ECF can only be initialized using the configuration file - it
 * is not recommended to initialize it directly through code. */
void write_ecf_configuration_file(const char* filename,
                                  const AlgorithmConfiguration& alg_config);

}

#endif
