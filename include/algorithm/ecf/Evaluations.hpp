#ifndef ALGORITHM_ECF_EVALUATIONS_HPP
#define ALGORITHM_ECF_EVALUATIONS_HPP

#include <ECF.h>

#include "Configuration.hpp"

namespace ECF {

/** Base class for all layout evaluation operators. Abstracts away all ECF types
 * for extending evaluation operators. */
class LayoutEvaluateOperator : public EvaluateOp {
  public:
    /** Function required by the ECF. Serves as a wrapper around evaluation
     * function that accepts actual layout solution instead of generic gene
     * pointer. In other words, all this function does is communicate with ECF
     * and hence cannot be overridden.*/
    FitnessP evaluate(IndividualP individual) final;

    /** Evaluates a given solution. All evaluation operators have to override
     * this function. */
    virtual McDeval::Risk evaluate(const McDeval::Solution& solution) = 0;
};

/** Uses a single evaluator to evaluate solutions. */
class SingleEvaluator : public LayoutEvaluateOperator {
  private:
    std::unique_ptr<McDeval::Evaluator> evaluator;

  public:
    SingleEvaluator(const McDeval::Configuration& app_config,
                    const AlgorithmConfiguration& alg_config);

    /** Evaluates a solution using a single evaluator. */
    McDeval::Risk evaluate(const McDeval::Solution& solution) override;
};

/** Uses a linear combination of evaluators to evaluate solutions. */
class LinearCombination : public LayoutEvaluateOperator {
  private:
    std::vector<std::unique_ptr<McDeval::Evaluator>> evaluators;
    std::vector<double> coefficients;

  public:
    LinearCombination(const McDeval::Configuration& app_config,
                      const AlgorithmConfiguration& alg_config);

    /** Evaluates a solution using linear combination of defined evaluators. */
    McDeval::Risk evaluate(const McDeval::Solution& solution) override;
};

}

#endif
