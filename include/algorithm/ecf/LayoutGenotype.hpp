#ifndef ALGORITHM_ECF_LAYOUTGENOTYPE_HPP
#define ALGORITHM_ECF_LAYOUTGENOTYPE_HPP

#include <McDeval.hpp>
#include <ECF.h>

namespace ECF {

class LayoutGenotype : public Genotype {
  public:
    McDeval::Solution solution;

  private:
    const McDeval::Configuration& app_config;

  public:
    explicit LayoutGenotype(const McDeval::Configuration& app_config);

    LayoutGenotype* copy() override;

    std::vector<CrossoverOpP> getCrossoverOp() override;

    std::vector<MutationOpP> getMutationOp() override;

    bool initialize(StateP state) override;

    void write(XMLNode& xMyGenotype) override;
    void read(XMLNode& xMyGenotype) override;
};
using LayoutGenotypeP = boost::shared_ptr<LayoutGenotype>;

}

#endif
