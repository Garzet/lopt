#ifndef ALGORITHM_ECF_CROSSOVERS_HPP
#define ALGORITHM_ECF_CROSSOVERS_HPP

#include <McDeval.hpp>
#include <ECF.h>

namespace ECF {

/** Base class for all layout crossover operators. Abstracts away all ECF types
 * for extending crossover operators. */
class LayoutCrossoverOperator : public CrossoverOp {
  protected:
    const McDeval::Configuration& app_config;

  public:
    explicit LayoutCrossoverOperator(const McDeval::Configuration& app_config);

    /** Function required by the ECF. Serves as a wrapper around crossover
     * function that accepts actual layout solutions instead of generic gene
     * pointers. In other words, all this function does is communicate with ECF
     * and hence cannot be overridden. */
    bool mate(GenotypeP gen1, GenotypeP gen2, GenotypeP child) final;

    /** Creates a child solution based on two parent solutions. All crossover
     * operators have to override this function. */
    virtual void mate(const McDeval::Solution& s1,
                      const McDeval::Solution& s2,
                      McDeval::Solution& child) = 0;
};

/** Builds a child by picking each part of the solution from one of the parents
 * randomly. */
class EachPartFromRandomParent : public LayoutCrossoverOperator {
  public:
    explicit EachPartFromRandomParent(const McDeval::Configuration& app_config);

    /** Construct child by getting the server of each component and each
     * fragment randomly from first or second parent.*/
    void mate(const McDeval::Solution& s1,
              const McDeval::Solution& s2,
              McDeval::Solution& child) override;
};

}
#endif
