#ifndef ALGORITHM_OPENGA_SINGLEOBJECTIVEGENETICALGORITHM_HPP
#define ALGORITHM_OPENGA_SINGLEOBJECTIVEGENETICALGORITHM_HPP

#include <openga/openga.hpp>

#include "algorithm/Algorithm.hpp"

namespace OpenGA {

class SingleObjectiveGeneticAlgorithm : public SingleObjectiveAlgorithm {
  private:
    /** Genetic algorithm object. */
    EA::Genetic<McDeval::Solution, McDeval::Risk> ga;

    /** Best solution of the latest run. */
    std::unique_ptr<McDeval::Solution> best;

  public:
    SingleObjectiveGeneticAlgorithm(const AlgorithmConfiguration& alg_config,
                                    const McDeval::Configuration& app_config);

    McDeval::Solution run() override;

    McDeval::Risk get_best_solution_risk() const override;

  private:
    /** Creates evaluator objects used for single objective optimization. If
     * only one is defined in algorithm configuration object, single resource
     * evaluator is created since it does not require coefficient to be
     * provided. If more than one evaluators are provided, linear combination is
     * used to combine them in a single evaluator. Therefore, coefficients are
     * mandatory when more then one evaluator is provided. */
    void create_evaluators();
};

}

#endif
