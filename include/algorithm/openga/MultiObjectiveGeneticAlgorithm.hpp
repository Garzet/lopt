#ifndef ALGORITHM_OPENGA_MULTIOBJECTIVEGENETICALGORITHM_HPP
#define ALGORITHM_OPENGA_MULTIOBJECTIVEGENETICALGORITHM_HPP

#include <openga/openga.hpp>

#include "algorithm/Algorithm.hpp"

namespace OpenGA {

class MultiObjectiveGeneticAlgorithm : public MultiObjectiveAlgorithm {
  private:
    /** Genetic algorithm object. */
    EA::Genetic<McDeval::Solution, std::vector<McDeval::Risk>> ga;

    /** Pareto solutions of the latest run. */
    std::vector<McDeval::Solution> pareto_solutions;

  public:
    MultiObjectiveGeneticAlgorithm(const AlgorithmConfiguration& alg_config,
                                   const McDeval::Configuration& app_config);

    std::vector<McDeval::Solution> run() override;

    std::vector<std::vector<McDeval::Risk>>
    get_nondominated_solution_risks() const override;

  private:
    /** Makes sure that parameters required by multi objective genetic algorithm
     * implemented within OpenGA library (NSGA-III) are set within the algorithm
     * configuration object. If a parameter is missing, runtime error is
     * thrown. This only checks for parameters that are specific to the NSGA-III
     * algorithm since many checks are done in the multi objective algorithm
     * super class. */
    void validate_parameters() const;
};

}

#endif
