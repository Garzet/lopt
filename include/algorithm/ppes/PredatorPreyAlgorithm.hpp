#ifndef ALGORITHM_PPES_PREDATORPREYALGORITHM_HPP
#define ALGORITHM_PPES_PREDATORPREYALGORITHM_HPP

#include <PPES.hpp>

#include "algorithm/Algorithm.hpp"

namespace PPES {

class PredatorPreyAlgorithm : public MultiObjectiveAlgorithm {
  private:
    /** Objectives used by the Predator-Prey algoritm to evaluate solutions. */
    std::vector<std::unique_ptr<Objective<McDeval::Solution>>> objs;

    /** Mutations used to mutate solutions. */
    std::vector<std::unique_ptr<Mutation<McDeval::Solution>>> muts;

    /** Predator-Prey algorithm configuration. Has to live as long as algorithm
     * object lives. */
    std::unique_ptr<Configuration> ppes_config;

    std::unique_ptr<Algorithm<McDeval::Solution>> algorithm;

  public:
    PredatorPreyAlgorithm(const AlgorithmConfiguration& alg_config,
                          const McDeval::Configuration& app_config);

    std::vector<McDeval::Solution> run() override;

    std::vector<std::vector<McDeval::Risk>>
    get_nondominated_solution_risks() const override;

  private:
    /** Makes sure that parameters required by predator prey algorithm are set
     * within the algorithm configuration object. If a parameter is missing,
     * runtime error is thrown. This only checks for parameters that are
     * specific to the predator prey algorithm since many checks are done in the
     * multi objective algorithm super class.  */
    void validate_parameters() const;
};

}

#endif
