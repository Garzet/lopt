#ifndef ALGORITHM_PPES_MUTATIONS_HPP
#define ALGORITHM_PPES_MUTATIONS_HPP

#include <McDeval.hpp>
#include <PPES.hpp>

namespace PPES {

class LayoutMutationOperator : public Mutation<McDeval::Solution> {
  protected:
    const McDeval::Configuration& app_config;

  public:
    LayoutMutationOperator(const std::string& name,
                           const McDeval::Configuration& app_config);

    void mutate(const McDeval::Solution& src,
                McDeval::Solution& dest) const override = 0;
};

/** Mutation operator that completely randomizes the solution. */
class RandomSolution : public LayoutMutationOperator {
  public:
    explicit RandomSolution(const McDeval::Configuration& app_config);

    /** Setts the destination to a newly generated random sulution. Source is
     * ignored when this mutation is performed. */
    void mutate(const McDeval::Solution& src,
                McDeval::Solution& dest) const override;
};

/** Mutation operator that reassigns a random assignable object (fragment or
 * component) to a random server. */
class RandomAssignableToRandomServer : public LayoutMutationOperator {
  public:
    explicit RandomAssignableToRandomServer(
        const McDeval::Configuration& app_config);

    /** Reassigns randomly chosen assignable object (fragment or component) to a
     * randomly chosen server. Source solution is copied into destination and
     * then mutation is performed on destination solution. */
    void mutate(const McDeval::Solution& src,
                McDeval::Solution& dest) const override;
};

}

#endif
