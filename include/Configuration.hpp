#ifndef CONFIGURATION_HPP
#define CONFIGURATION_HPP

#include <McDeval.hpp>

#include <array>
#include <optional>
#include <vector>

struct EvaluatorData {
    McDeval::Evaluator::Type evaluator_type = {};
    std::optional<double> coefficient;
    std::optional<std::array<double, 2>> plot_range;

    friend std::ostream& operator<<(std::ostream& s, const EvaluatorData& ed);
};

class AlgorithmConfiguration {
  public:
    std::string type;
    std::optional<unsigned> population_size;
    std::optional<unsigned> world_width;
    std::optional<unsigned> world_height;
    std::optional<unsigned> predators_per_objective;
    std::optional<unsigned> generation_max;
    std::optional<unsigned> log_frequency;
    std::optional<unsigned> plot_frequency;
    std::optional<bool> pause_after_plot;
    std::optional<double> mutation_rate;
    std::vector<EvaluatorData> evaluators;

  public:
    friend std::ostream& operator<<(std::ostream& s,
                                    const AlgorithmConfiguration& alg_config);
};

#endif
