#ifndef GNUPLOT_HPP
#define GNUPLOT_HPP

#include <cstdio>
#include <string>

class Gnuplot final {
  private:
    /** Operating system pipe used to interact with gnuplot. */
    FILE* pipe;

  public:
    /** Tries start a gnuplot process and open a pipe to it so commands can be
     * sent. Persist option allows gnuplot to stay open even when this process
     * has finished. Throws gnuplot exception if pipe to a gnuplot process
     * failed to open or gnuplot is not available on the system. */
    explicit Gnuplot(bool persist = true);

    /** Closes the gnuplot pipe if it was successfully open. */
    ~Gnuplot();

    /** Disable copy construction of gnuplot objects. */
    Gnuplot(const Gnuplot& other) = delete;

    /** Enable moving of gnuplot objects. */
    Gnuplot(Gnuplot&& other) noexcept;

    /** Disable assignment of gnuplot objects. */
    Gnuplot& operator=(const Gnuplot& other) = delete;

    /** Enable moving of gnuplot objects. */
    Gnuplot& operator=(Gnuplot&& other) noexcept;

    /** Sends an arbitrary string to gnuplot. Note for large amounts of data,
     * it is probably wise to build the string first (for example, using a
     * string stream) and then send it to gnuplot to improve performance. */
    Gnuplot& operator<<(const std::string& s);

    /** Sends an arbitrary character to gnuplot. */
    Gnuplot& operator<<(char c);

    /** Flushes the gnuplot pipe. This should be called whenever commands
     * have been issued and response from gnuplot is expected. This is required
     * since pipe is buffered. */
    void flush();
};

class GnuplotException : public std::exception {
  private:
    std::string message;

  public:
    explicit GnuplotException(std::string message) noexcept;

    const char* what() const noexcept override;
};

#endif
