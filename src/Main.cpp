#include "Parser.hpp"
#include "Log.hpp"

#include "algorithm/ecf/SingleObjectiveGeneticAlgorithm.hpp"
#include "algorithm/openga/MultiObjectiveGeneticAlgorithm.hpp"
#include "algorithm/openga/SingleObjectiveGeneticAlgorithm.hpp"
#include "algorithm/ppes/PredatorPreyAlgorithm.hpp"

#include <unordered_set>

int main(int argc, char* argv[])
{
    try {
        // Make sure two command line arguments have been provided.
        if (argc < 3) {
            throw std::runtime_error(fmt::format(
                "Usage: {} app_config.xml alg_config.xml", argv[0])); // NOLINT
        }

        // Parse application configuration file.
        McDeval::Configuration app_config =
            Parser::load_application_configuration(argv[1]); // NOLINT
        { // Configuration file validation and logging.
            app_config.validate();
            Log::success("Application configuration loaded:\n{}", app_config);
        }

        // Parse algorithm configuration file.
        AlgorithmConfiguration alg_config =
            Parser::load_algorithm_configuration(argv[2]); // NOLINT
        { // Configuration file validation and logging.
            std::stringstream ss;
            Log::success("Algorithm configuration loaded:\n{}", alg_config);
        }

        std::stringstream ss; // Stream for reporting algorithm results.
        if (auto soa =
                SingleObjectiveAlgorithm::create(alg_config, app_config)) {
            McDeval::Solution s = soa->run();
            ss << "Best solution (risk = " << soa->get_best_solution_risk()
               << ")\n"
               << s << '\n';
        } else if (std::unique_ptr<MultiObjectiveAlgorithm> moa =
                       MultiObjectiveAlgorithm::create(alg_config,
                                                       app_config)) {
            std::vector<McDeval::Solution> sols = moa->run();
            std::vector<std::vector<McDeval::Risk>> ndsr =
                moa->get_nondominated_solution_risks();

            std::set<std::vector<McDeval::Risk>> s; // Keep only unique risks.
            for (const std::vector<McDeval::Risk>& rv : ndsr) { s.insert(rv); }
            for (const std::vector<McDeval::Risk>& s_risks : s) {
                ss << "{  ";
                for (auto&& risk : s_risks) { ss << risk << "  "; }
                ss << "}\n";
            }
            ss << "\n---------- SOLUTIONS ----------\n";
            // Set to keep unique solutions.
            std::unordered_set<McDeval::Solution> unique_sols;
            for (auto&& s : sols) { unique_sols.insert(s); }
            for (auto it = unique_sols.begin(); it != unique_sols.end(); ++it) {
                ss << *it;
                if (std::distance(it, unique_sols.end()) != 1) { ss << '\n'; }
            }
            ss << "-------------------------------\n";
        } else {
            throw std::runtime_error("No algorithm with provided type.");
        }
        Log::success("{}", ss.str()); // Report algorithm has finished.
    } catch (const std::exception& exc) {
        Log::error(exc.what());
        return EXIT_FAILURE;
    }
}
