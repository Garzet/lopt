#include "Parser.hpp"

#include <rapidxml/rapidxml.hpp>
#include <fmt/format.h>
#include <fmt/ostream.h>

#include <algorithm>
#include <fstream>
#include <string>
#include <unordered_set>

namespace {

std::vector<char> load_file_data(const std::string& filename);

void extract_resources_node(rapidxml::xml_document<>& dc,
                            McDeval::Configuration& cfg);
void extract_fragment_node(rapidxml::xml_node<>* f,
                           McDeval::Configuration& cfg);
void extract_result_node(rapidxml::xml_node<>* r, McDeval::Configuration& cfg);

void extract_application_node(rapidxml::xml_document<>& dc,
                              McDeval::Configuration& cfg);
void extract_component_node(rapidxml::xml_node<>* c,
                            McDeval::Configuration& cfg);
std::vector<std::string>
load_component_inputs(rapidxml::xml_node<>* c,
                      const McDeval::Configuration& cfg);
std::vector<std::string>
load_component_outputs(rapidxml::xml_node<>* c,
                       const McDeval::Configuration& cfg);

void set_resource_source_and_destination(rapidxml::xml_document<>& dc,
                                         McDeval::Configuration& cfg);
void set_resource_source_and_destination_for_component(
    rapidxml::xml_node<>* c, McDeval::Configuration& cfg);

void extract_deployment_node(rapidxml::xml_document<>& dc,
                             McDeval::Configuration& cfg);

void extract_security_node(rapidxml::xml_document<>& dc,
                           McDeval::Configuration& cfg);
std::pair<std::vector<const McDeval::Fragment*>,
          std::vector<const McDeval::Result*>>
load_constraint_resources(rapidxml::xml_node<>* c,
                          const McDeval::Configuration& cfg);

unsigned parse_unsigned(const std::string& val, const std::string& name);
double parse_double(const std::string& val, const std::string& name);
void validate_id_attribute(const rapidxml::xml_attribute<>* id_attr,
                           const std::string& id_owner_name);

template<typename T>
void load_optional(std::optional<T>& target,
                   rapidxml::xml_node<>* parent,
                   const char* name,
                   const char* log_name);

template<>
void load_optional<unsigned>(std::optional<unsigned>& target,
                             rapidxml::xml_node<>* parent,
                             const char* name,
                             const char* log_name);
template<>
void load_optional<double>(std::optional<double>& target,
                           rapidxml::xml_node<>* parent,
                           const char* name,
                           const char* log_name);
template<>
void load_optional<bool>(std::optional<bool>& target,
                         rapidxml::xml_node<>* parent,
                         const char* name,
                         const char* log_name);

void extract_evaluator_nodes(rapidxml::xml_node<>* evs,
                             AlgorithmConfiguration& cfg);

void load_plot_range(EvaluatorData& ed, rapidxml::xml_node<>* evaluator_node);
}

namespace Parser {

McDeval::Configuration
load_application_configuration(const std::string& app_config_filename)
{
    // Characters from the XML configuration file.
    std::vector<char> data = load_file_data(app_config_filename);

    rapidxml::xml_document<> doc;
    doc.parse<0>(&data[0]); // Parse XML document string with default settings.

    McDeval::Configuration app_config;
    extract_resources_node(doc, app_config);
    extract_application_node(doc, app_config);

    set_resource_source_and_destination(doc, app_config);

    extract_deployment_node(doc, app_config);
    extract_security_node(doc, app_config);

    return app_config;
}

AlgorithmConfiguration
load_algorithm_configuration(const std::string& alg_config_filename)
{
    // Characters from the XML configuration file.
    std::vector<char> data = load_file_data(alg_config_filename);

    rapidxml::xml_document<> doc;
    doc.parse<0>(&data[0]); // Parse XML document string with default settings.

    AlgorithmConfiguration alg_config;

    auto* a = doc.first_node("algorithm");
    if (!a) {
        throw std::logic_error("No algorithm node in configuration file.");
    }

    { // Load algorithm type (mandatory - perform some checks here).
        auto* t = a->first_node("type");
        if (!t) {
            throw std::logic_error("Algorithm node contains no type node.");
        }
        if (t->value_size() == 0) {
            throw std::logic_error("Algorithm type cannot be empty string.");
        }
        alg_config.type = t->value();
    }

    // Load population size.
    load_optional<unsigned>(
        alg_config.population_size, a, "population_size", "population size");
    // Load world width.
    load_optional<unsigned>(
        alg_config.world_width, a, "world_width", "world width");
    // Load world height.
    load_optional<unsigned>(
        alg_config.world_height, a, "world_height", "world height");
    // Load number of predators per objective.
    load_optional<unsigned>(alg_config.predators_per_objective,
                            a,
                            "predators_per_objective",
                            "number of predators per objective");
    // Load max generations.
    load_optional<unsigned>(
        alg_config.generation_max, a, "generation_max", "max generations");
    // Load logging frequency.
    load_optional<unsigned>(
        alg_config.log_frequency, a, "log_frequency", "logging frequency");
    // Load plotting frequency.
    load_optional<unsigned>(
        alg_config.plot_frequency, a, "plot_frequency", "plotting frequency");
    // Load pause after plotting flag.
    load_optional<bool>(alg_config.pause_after_plot,
                        a,
                        "pause_after_plot",
                        "pause after plotting flag");
    // Load mutation rate.
    load_optional<double>(
        alg_config.mutation_rate, a, "mutation_rate", "mutation rate");

    { // Load evaluator names.
        auto* evs = a->first_node("evaluators");
        if (!evs) {
            throw std::logic_error("Algorithm node contains no evaluators "
                                   "node");
        }
        extract_evaluator_nodes(evs, alg_config);
    }

    return alg_config;
}

}

namespace {

std::vector<char> load_file_data(const std::string& filename)
{
    // Characters from the XML configuration file.
    std::vector<char> data;

    // Open stream.
    std::basic_ifstream<char> stream(filename, std::ios::binary);
    if (!stream) { throw std::runtime_error("Cannot open file: " + filename); }
    stream.unsetf(std::ios::skipws);

    // Determine stream size.
    stream.seekg(0, std::ios::end);
    size_t size = stream.tellg();
    stream.seekg(0);

    // Load data and add termination character.
    data.resize(size + 1);
    stream.read(&data.front(), static_cast<std::streamsize>(size));
    data[size] = '\0';

    return data;
}

void extract_resources_node(rapidxml::xml_document<>& dc,
                            McDeval::Configuration& cfg)
{
    // Get the resources node.
    auto* r = dc.first_node("resources");
    if (!r) {
        throw std::logic_error("No resources node in configuration file.");
    }

    // Get the first fragment and make sure at least one fragment is given.
    auto* f = r->first_node("fragment");
    if (!f) {
        throw std::logic_error("Resources node contains no fragment nodes.");
    }
    for (; f; f = f->next_sibling("fragment")) { // Iterate over fragments.
        extract_fragment_node(f, cfg); // Load fragment to the configuration.
    }

    // Get the first result. No result nodes is acceptable.
    for (auto* x = r->first_node("result"); x; x = x->next_sibling("result")) {
        extract_result_node(x, cfg); // Load result to the configuration.
    }
}

void extract_fragment_node(rapidxml::xml_node<>* f, McDeval::Configuration& cfg)
{
    rapidxml::xml_attribute<>* fid = f->first_attribute("ID");
    validate_id_attribute(fid, "Fragment");

    // Get fragment importance.
    auto* imn = f->first_node("importance");
    if (!imn) {
        throw std::logic_error("Fragment node must contain importance "
                               "node.");
    }
    unsigned importance = parse_unsigned(imn->value(), "fragment importance");

    // Create fragment with provided ID and importance.
    cfg.fragments.emplace_back(fid->value(), importance);
}

void extract_result_node(rapidxml::xml_node<>* res, McDeval::Configuration& cfg)
{
    rapidxml::xml_attribute<>* rid = res->first_attribute("ID");
    validate_id_attribute(rid, "Result");

    // Get fragment importance.
    auto* imn = res->first_node("importance");
    if (!imn) {
        throw std::logic_error("Result node must contain importance node.");
    }
    unsigned importance = parse_unsigned(imn->value(), "result importance");

    // Create result with provided ID and importance.
    cfg.results.emplace_back(rid->value(), importance);
}

void extract_application_node(rapidxml::xml_document<>& dc,
                              McDeval::Configuration& cfg)
{
    // Get the application node.
    auto* a = dc.first_node("application");
    if (!a) {
        throw std::logic_error("No application node in configuration file.");
    }

    // Get first component node.
    auto* c = a->first_node("component");
    if (!c) {
        throw std::logic_error("Application node must contain at least one "
                               "component node.");
    }
    for (; c; c = c->next_sibling("component")) { // Component nodes.
        extract_component_node(c, cfg); // Load component to configuration.
    }
}

void extract_component_node(rapidxml::xml_node<>* c,
                            McDeval::Configuration& cfg)
{
    // Load component ID.
    rapidxml::xml_attribute<>* cid = c->first_attribute("ID");
    validate_id_attribute(cid, "Component");

    std::vector<const McDeval::Resource*>
        inputs; // Pointers to input resources.
    std::vector<std::string> input_ids = load_component_inputs(c, cfg);
    {
        // IDs of accessed resources. Component inputs.
        inputs.reserve(input_ids.size());

        { // Create pointers to fragment objects from loaded fragment IDs.
            auto& fs = cfg.fragments; // Shortcut reference.
            for (auto&& iid : input_ids) {
                auto iter = std::find(fs.begin(), fs.end(), iid);
                if (iter != fs.end()) { inputs.push_back(&*iter); }
            }
        }
        { // Create pointers to result objects from loaded result IDs.
            auto& rs = cfg.results; // Shortcut reference.
            for (auto&& iid : input_ids) {
                auto iter = std::find(rs.begin(), rs.end(), iid);
                if (iter != rs.end()) { inputs.push_back(&*iter); }
            }
        }
    }

    std::vector<const McDeval::Result*> outputs; // Pointers to output results.
    std::vector<std::string> output_ids = load_component_outputs(c, cfg);
    {
        // IDs of output results. Component outputs.
        outputs.reserve(output_ids.size());

        { // Create pointers to fragment objects from loaded fragment IDs.
            auto& rs = cfg.results; // Shortcut reference.
            for (auto&& oid : output_ids) {
                auto iter = std::find(rs.begin(), rs.end(), oid);
                if (iter != rs.end()) { outputs.push_back(&*iter); }
            }
        }
    }

    // Make sure that component does not have same resource as input and output.
    for (auto&& input_id : input_ids) {
        if (std::find(output_ids.begin(), output_ids.end(), input_id) !=
            output_ids.end()) { // ID is both in both outputs and inputs.
            throw std::runtime_error("Resource cannot be both input and "
                                     "output of a single component.");
        }
    }

    // Create component from ID and pointers to accessed fragments.
    cfg.components.emplace_back(cid->value(), inputs, outputs);
}

std::vector<std::string>
load_component_inputs(rapidxml::xml_node<>* c,
                      const McDeval::Configuration& cfg)
{
    std::vector<std::string> inputs; // Inputs to this component. Can be empty.

    for (auto* i = c->first_node("input"); i; i = i->next_sibling("input")) {
        inputs.emplace_back(i->value());
    }

    // Check if all resource ID in input list are valid.
    const auto& fs = cfg.fragments; // Shortcut reference.
    const auto& rs = cfg.results;   // Shortcut reference.
    for (auto&& iid : inputs) {     // Iterate over input IDs.
        // Check if input with given ID exists in fragments or results vector.
        if (std::find(fs.begin(), fs.end(), iid) == fs.end() &&
            std::find(rs.begin(), rs.end(), iid) == rs.end()) {
            throw std::runtime_error("Component references unknown resource as "
                                     "input.");
        }
    }

    // Make sure all IDs of accessed resources are unique. Use unordered set to
    // kill duplicate IDs.
    std::unordered_set<std::string> unique_input_ids(inputs.begin(),
                                                     inputs.end());
    if (unique_input_ids.size() != inputs.size()) {
        throw std::logic_error("Component must access each resource only "
                               "once.");
    }

    return inputs;
}

std::vector<std::string>
load_component_outputs(rapidxml::xml_node<>* c,
                       const McDeval::Configuration& cfg)
{
    std::vector<std::string>
        outputs; // Outputs of this component. Can be empty.
    for (auto* o = c->first_node("output"); o; o = o->next_sibling("output")) {
        outputs.emplace_back(o->value());
    }

    // Check if all resource ID in output list are valid.
    const auto& rs = cfg.results; // Shortcut reference.
    for (auto&& oid : outputs) {  // Iterate over output IDs.
        // Check if output with given ID exists in results vector.
        if (std::find(rs.begin(), rs.end(), oid) == rs.end()) {
            // throw std::runtime_error(fmt::format(
            //"Component references unknown result ({} ass output.", oid));
        }
    }

    // Make sure all IDs of outputs are unique. Use unordered set to kill
    // duplicate IDs.
    std::unordered_set<std::string> unique_output_ids(outputs.begin(),
                                                      outputs.end());
    if (unique_output_ids.size() != outputs.size()) {
        throw std::logic_error("Component must reference each result as output "
                               "only once.");
    }

    return outputs;
}

void set_resource_source_and_destination(rapidxml::xml_document<>& dc,
                                         McDeval::Configuration& cfg)
{
    // Get the application node.
    auto* a = dc.first_node("application");
    if (!a) {
        throw std::logic_error("No application node in configuration file.");
    }

    // Get first component node.
    auto* c = a->first_node("component");
    if (!c) {
        throw std::logic_error("Application node must contain at least one "
                               "component node.");
    }
    for (; c; c = c->next_sibling("component")) { // Component nodes.
        set_resource_source_and_destination_for_component(c, cfg);
    }
}

void set_resource_source_and_destination_for_component(
    rapidxml::xml_node<>* c, McDeval::Configuration& cfg)
{
    // Load component ID.
    rapidxml::xml_attribute<>* cid = c->first_attribute("ID");
    validate_id_attribute(cid, "Component");

    // Find component with the provided ID in components vector.
    auto it =
        std::find(cfg.components.begin(), cfg.components.end(), cid->value());
    if (it == cfg.components.end()) {
        throw std::runtime_error("Component with provided ID could not be "
                                 "found in configuration object.");
    }
    const auto& component = *it;

    { // Link fragments that are inputs to this component by adding a pointer to
      // this component to internal vector of a fragment.
        std::vector<std::string> input_ids = load_component_inputs(c, cfg);
        auto& fs = cfg.fragments; // Shortcut reference.
        for (auto&& iid : input_ids) {
            auto iter = std::find(fs.begin(), fs.end(), iid);
            if (iter != fs.end()) { iter->add_destination(component); }
        }
    }

    { // Link results that are inputs to this component to this component by
        // setting the destination of the results.
        std::vector<std::string> input_ids = load_component_inputs(c, cfg);
        auto& rs                           = cfg.results; // Shortcut reference.
        for (auto&& iid : input_ids) {
            auto iter = std::find(rs.begin(), rs.end(), iid);
            if (iter != rs.end()) { iter->set_destination(component); }
        }
    }

    std::vector<McDeval::Result*> result_outputs;
    { // Link results that are outputs of this component to this component by
        // setting the source of the results.
        std::vector<std::string> output_ids = load_component_outputs(c, cfg);
        auto& rs = cfg.results; // Shortcut reference.
        for (auto&& oid : output_ids) {
            auto iter = std::find(rs.begin(), rs.end(), oid);
            if (iter != rs.end()) { iter->set_source(component); }
        }
    }
}

void extract_deployment_node(rapidxml::xml_document<>& dc,
                             McDeval::Configuration& cfg)
{
    // Get the deployment node.
    auto* a = dc.first_node("deployment");
    if (!a) {
        throw std::logic_error("No deployment node in configuration file.");
    }

    // Get first server node.
    auto* s = a->first_node("server");
    if (!s) {
        throw std::logic_error("Deployment node must contain at least one "
                               "server node.");
    }
    for (; s; s = s->next_sibling("server")) { // Server nodes.
        // Get the ID of the server.
        rapidxml::xml_attribute<>* sid = s->first_attribute("ID");
        if (!sid) {
            throw std::logic_error("Server node must have ID attribute.");
        }
        if (sid->value_size() == 0) {
            throw std::logic_error("Server ID cannot be empty string.");
        }

        // Get the trust of the server.
        auto* t = s->first_node("trust");
        if (!t) {
            throw std::logic_error("Server node must contain trust node.");
        }
        unsigned trust = parse_unsigned(t->value(), "server trust");

        // Create a server from configuration data.
        McDeval::Server ser(sid->value(), trust);

        // Make sure that the created server has not already been seen.
        if (std::find(cfg.servers.begin(), cfg.servers.end(), ser) !=
            cfg.servers.end()) {
            throw std::runtime_error(
                fmt::format("Server ID '{}' is not unique.", ser.id));
        }

        // Push server to configuration vector.
        cfg.servers.push_back(ser);
    }
}

void extract_security_node(rapidxml::xml_document<>& dc,
                           McDeval::Configuration& cfg)
{
    // Get the deployment node.
    auto* a = dc.first_node("security");
    if (!a) {
        throw std::logic_error("No security node in configuration file.");
    }

    // Get first constraint node.

    // It is acceptable that no constraints have been provided.
    for (auto* c = a->first_node("constraint"); c;
         c       = c->next_sibling("constraint")) {
        // Get the ID of the constraint.
        rapidxml::xml_attribute<>* cid = c->first_attribute("ID");
        validate_id_attribute(cid, "Constraint");

        // Load constraint resources.
        auto res = load_constraint_resources(c, cfg);

        // Create a security constraint from fragments and results.
        cfg.constraints.emplace_back(cid->value(), res.first, res.second);
    }
}

std::pair<std::vector<const McDeval::Fragment*>,
          std::vector<const McDeval::Result*>>
load_constraint_resources(rapidxml::xml_node<>* c,
                          const McDeval::Configuration& cfg)
{
    // Resources of this constraint.
    std::pair<std::vector<const McDeval::Fragment*>,
              std::vector<const McDeval::Result*>>
        res;

    for (auto* cf = c->first_node("resource"); cf;
         cf       = cf->next_sibling("resource")) {
        // Find resource in the fragments.
        auto it =
            std::find(cfg.fragments.begin(), cfg.fragments.end(), cf->value());
        if (it != cfg.fragments.end()) { // Resource found in fragments.
            res.first.push_back(&*it);
        } else { // Try to look into results for given resource.
            auto it =
                std::find(cfg.results.begin(), cfg.results.end(), cf->value());
            if (it != cfg.results.end()) { // Resource found in results.
                res.second.push_back(&*it);
            } else {
                throw std::runtime_error("Constraint references unknown "
                                         "resource.");
            }
        }
    }

    return res;
}

unsigned parse_unsigned(const std::string& val, const std::string& name)
{
    // Convert number string to a number.
    try {
        return std::stoul(val);
    } catch (const std::logic_error&) { // Failed to convert string to unsigned.
        throw std::logic_error("Given " + name +
                               " is not a valid unsigned "
                               "integer value.");
    }
}

double parse_double(const std::string& val, const std::string& name)
{
    try {
        return std::stod(val);
    } catch (const std::logic_error&) { // Failed to convert string to double.
        throw std::logic_error("Given " + name +
                               " is not a valid double "
                               "value.");
    }
}

void validate_id_attribute(const rapidxml::xml_attribute<>* id_attr,
                           const std::string& id_owner_name)
{
    if (!id_attr) {
        throw std::logic_error(
            fmt::format("{} node must have ID attribute", id_owner_name));
    }
    if (id_attr->value_size() == 0) {
        throw std::logic_error(
            fmt::format("{} ID cannot be empty string.", id_owner_name));
    }
}

template<>
void load_optional<unsigned>(std::optional<unsigned>& target,
                             rapidxml::xml_node<>* parent,
                             const char* name,
                             const char* log_name)
{
    auto* s = parent->first_node(name);
    if (s) { target = parse_unsigned(s->value(), log_name); }
}

template<>
void load_optional<double>(std::optional<double>& target,
                           rapidxml::xml_node<>* parent,
                           const char* name,
                           const char* log_name)
{
    auto* s = parent->first_node(name);
    if (s) { target = parse_double(s->value(), log_name); }
}

template<>
void load_optional<bool>(std::optional<bool>& target,
                         rapidxml::xml_node<>* parent,
                         const char* name,
                         const char* log_name)
{
    auto* s = parent->first_node(name);
    if (s) {
        std::string v = s->value();
        std::transform(v.begin(), v.end(), v.begin(), ::tolower);

        // Left trim.
        v.erase(v.begin(), std::find_if(v.begin(), v.end(), [](int ch) {
                    return !std::isspace(ch);
                }));

        // Right trim.
        v.erase(std::find_if(v.rbegin(),
                             v.rend(),
                             [](int ch) { return !std::isspace(ch); })
                    .base(),
                v.end());

        if (v == "true") {
            target = true;
        } else if (v == "false") {
            target = false;
        } else {
            throw std::runtime_error(fmt::format(
                "Provided {} is not a valid boolean value.", log_name));
        }
    }
}

void extract_evaluator_nodes(rapidxml::xml_node<>* evs,
                             AlgorithmConfiguration& cfg)
{
    // Make sure at least one evaluator name has been provided.
    auto* e = evs->first_node("evaluator");
    if (!e) {
        throw std::logic_error("Evaluators node contains no evaluator nodes");
    }

    for (; e; e = e->next_sibling("evaluator")) {
        // Extract evaluator name.
        auto* n = e->first_attribute("name");
        if (!n) {
            throw std::logic_error("Evaluator node contains no name "
                                   "attribute.");
        }
        if (n->value_size() == 0) {
            throw std::logic_error("Evaluator name cannot be an empty string.");
        }
        EvaluatorData ed;

        // Extract evaluator type from the name.
        auto evaluator_name = std::string(n->value());
        ed.evaluator_type   = evaluator_name == "Constraint" ?
                                McDeval::Evaluator::Type::Constraint :
                                evaluator_name == "Single Resource" ?
                                McDeval::Evaluator::Type::SingleResource :
                                evaluator_name == "Resident" ?
                                McDeval::Evaluator::Type::Resident :
                                throw std::runtime_error(
                                    "Unknown evaluator name:" + evaluator_name);

        load_optional<double>(
            ed.coefficient, e, "coefficient", "evaluator coefficient");
        load_plot_range(ed, e);
        cfg.evaluators.push_back(ed);
    }
}

void load_plot_range(EvaluatorData& ed, rapidxml::xml_node<>* evaluator_node)
{
    auto* n = evaluator_node->first_node("plot_range");
    if (n) {
        auto* la = n->first_attribute("lower");
        if (la) {
            std::array<double, 2> pr = {}; // Plot range.
            pr[0]    = parse_double(la->value(), "lower plot bound");
            auto* ua = n->first_attribute("upper");
            if (!ua) {
                throw std::runtime_error("Lower bound of a plot range is given,"
                                         " but upper is not.");
            }
            pr[1]         = parse_double(ua->value(), "upper plot bound");
            ed.plot_range = pr;
        }
    }
}

}
