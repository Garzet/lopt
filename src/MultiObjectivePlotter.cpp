#include "MultiObjectivePlotter.hpp"

#include "Log.hpp"

#include <sstream>

Plotter2D::Plotter2D(std::array<double, 2> x_range,
                     std::array<double, 2> y_range)
    : gp(nullptr)
{
    // Try to create gnuplot object. Checks weather gnuplot is installed, tries
    // to open a gnuplot process and open a pipe to it. If any part of that
    // process fails, plotting is disabled and warning is logged.
    try {
        gp = std::make_unique<Gnuplot>(); // Create gnuplot object.
    } catch (const GnuplotException& exc) {
        gp = nullptr; // Null pointer signals that plotting is disabled.
        Log::warn("Plotting is disabled for the following reason: {}\n",
                  exc.what());
        return;
    }

    *gp << "set grid\n"
        << "unset key\n"
        << "set style line 1 pointtype 1 pointsize 2\n"
        << "set xrange [" << std::to_string(x_range[0]) << ':'
        << std::to_string(x_range[1]) << "]\n"
        << "set yrange [" << std::to_string(y_range[0]) << ':'
        << std::to_string(y_range[1]) << "]\n";
}

void Plotter2D::plot(const std::vector<std::pair<double, double>>& points)
{
    if (!gp) { return; } // Check if plotting is disabled.

    std::stringstream ss;
    ss << "plot '-' linestyle 1\n";
    for (const std::pair<double, double>& point : points) {
        ss << point.first << ' ' << point.second << '\n';
    }
    ss << "e\n";     // Mark end of data.
    *gp << ss.str(); // Send plot command and data to gnuplot.
    gp->flush();     // Flush the gnuplot pipe. Graph should appear.
}

void Plotter2D::plot(const std::vector<std::array<double, 2>>& points)
{
    if (!gp) { return; } // Check if plotting is disabled.

    std::stringstream ss;
    ss << "plot '-' linestyle 1\n";
    for (const std::array<double, 2>& point : points) {
        ss << point[0] << ' ' << point[1] << '\n';
    }
    ss << "e\n";     // Mark end of data.
    *gp << ss.str(); // Send plot command and data to gnuplot.
    gp->flush();     // Flush the gnuplot pipe. Graph should appear.
}

void Plotter2D::plot(const std::vector<std::vector<double>>& points)
{
    if (!gp) { return; } // Check if plotting is disabled.

    std::stringstream ss;
    ss << "plot '-' linestyle 1\n";
    for (const std::vector<double>& point : points) {
        if (point.size() != 2) { // Make sure point is two dimensional.
            throw std::runtime_error("Two dimensional plotter can only draw "
                                     "two dimensional points.");
        }
        ss << point[0] << ' ' << point[1] << '\n';
    }
    ss << "e\n";     // Mark end of data.
    *gp << ss.str(); // Send plot command and data to gnuplot.
    gp->flush();     // Flush the gnuplot pipe. Graph should appear.
}
