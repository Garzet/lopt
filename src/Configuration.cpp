#include "Configuration.hpp"

#include <ostream>

std::ostream& operator<<(std::ostream& s,
                         const AlgorithmConfiguration& alg_config)
{
    s << "Algorithm type:          "
      << (alg_config.type.empty() ? "<NONE>" : alg_config.type) << '\n';

    if (alg_config.generation_max.has_value()) {
        s << "Number of generations:   " << *alg_config.generation_max << '\n';
    }

    if (alg_config.world_width.has_value()) {
        s << "World width:             " << *alg_config.world_width << '\n';
    }

    if (alg_config.world_height.has_value()) {
        s << "World height:            " << *alg_config.world_height << '\n';
    }

    if (alg_config.predators_per_objective.has_value()) {
        s << "Predators per objective: " << *alg_config.predators_per_objective
          << '\n';
    }

    if (alg_config.population_size.has_value()) {
        s << "Population size:         " << *alg_config.population_size << '\n';
    }

    if (alg_config.log_frequency.has_value()) {
        s << "Log frequency:           " << *alg_config.log_frequency << '\n';
    }

    if (alg_config.plot_frequency.has_value()) {
        s << "Plot frequency:          " << *alg_config.plot_frequency << '\n';
    }

    if (alg_config.pause_after_plot.has_value()) {
        s << "Pause after plot:        "
          << (*alg_config.pause_after_plot ? "true" : "false") << '\n';
    }

    if (alg_config.mutation_rate.has_value()) {
        s << "Mutation rate:           " << *alg_config.mutation_rate << '\n';
    }

    if (!alg_config.evaluators.empty()) {
        s << "Evaluators:              ";
        for (size_t i = 0, n = alg_config.evaluators.size(); i < n; i++) {
            s << alg_config.evaluators[i];
            // If this is not the last evaluator, print new line and spaces.
            if (i != n - 1) { s << "\n                         "; }
        }
        s << '\n';
    }

    return s;
}

std::ostream& operator<<(std::ostream& s, const EvaluatorData& ed)
{
    s << McDeval::Evaluator::type_to_string(ed.evaluator_type);
    if (ed.coefficient.has_value()) { s << " | " << *ed.coefficient; }
    if (ed.plot_range.has_value()) {
        s << " | {" << ed.plot_range.value()[0] << ", "
          << ed.plot_range.value()[1] << '}';
    }
    return s;
}
