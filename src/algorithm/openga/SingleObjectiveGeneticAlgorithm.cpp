#include "algorithm/openga/SingleObjectiveGeneticAlgorithm.hpp"

#include "Random.hpp"

namespace OpenGA {

namespace {
void init_genes(McDeval::Solution& s, const std::function<double(void)>& rnd01);

bool eval_solution(const McDeval::Solution& sol, McDeval::Risk& risk);

McDeval::Solution mutate(const McDeval::Solution& s,
                         const std::function<double(void)>& rnd01,
                         double shrink_scale);

McDeval::Solution crossover(const McDeval::Solution& s1,
                            const McDeval::Solution& s2,
                            const std::function<double(void)>& rnd01);

double calculate_total_fitness(
    const EA::Genetic<McDeval::Solution, McDeval::Risk>::thisChromosomeType& x);

void report_generation(
    int generation_number,
    const EA::GenerationType<McDeval::Solution, McDeval::Risk>& last_generation,
    const McDeval::Solution& best_genes);

class OpenGASingleton {
  public:
    const McDeval::Configuration* app_config_p = nullptr;
    unsigned log_frequency                     = 1;

    /** Evaluators and their coefficients. */
    std::vector<std::pair<std::unique_ptr<McDeval::Evaluator>, double>>
        evaluators;

    static OpenGASingleton& get()
    {
        static OpenGASingleton open_ga_singleton;
        return open_ga_singleton;
    }
};
}

SingleObjectiveGeneticAlgorithm::SingleObjectiveGeneticAlgorithm(
    const AlgorithmConfiguration& alg_config,
    const McDeval::Configuration& app_config)
    : SingleObjectiveAlgorithm(alg_config, app_config)
{
    // Setting static objects so they can be accessed by static functions.
    auto&& ogas        = OpenGASingleton::get();
    ogas.app_config_p  = &app_config;
    ogas.log_frequency = alg_config.log_frequency.value();
    create_evaluators();

    // Algorithm object configuration.
    ga.problem_mode       = EA::GA_MODE::SOGA;
    ga.generation_max     = alg_config.generation_max.value();
    ga.population         = alg_config.population_size.value();
    ga.mutation_rate      = alg_config.mutation_rate.value();
    ga.crossover_fraction = 1.0 - alg_config.mutation_rate.value();

    constexpr unsigned elite_ratio = 100;
    ga.elite_count =
        static_cast<int>(alg_config.population_size.value() / elite_ratio);

    // Register functions used by the algorithm.
    ga.init_genes                 = init_genes;
    ga.calculate_SO_total_fitness = calculate_total_fitness;
    ga.init_genes                 = init_genes;
    ga.eval_solution              = eval_solution;
    ga.mutate                     = mutate;
    ga.crossover                  = crossover;
    ga.SO_report_generation       = report_generation;

    // Disable stopping when best or average stalls. Only use max generations.
    ga.best_stall_max    = std::numeric_limits<int>::max();
    ga.average_stall_max = std::numeric_limits<int>::max();

    // Disable multi-threading since evaluation is relatively short.
    ga.multi_threading = false;
}

McDeval::Solution SingleObjectiveGeneticAlgorithm::run()
{
    ga.solve();

    auto best_index = ga.last_generation.best_chromosome_index;
    McDeval::Solution best_sol =
        ga.last_generation.chromosomes[best_index].genes;

    best = std::make_unique<McDeval::Solution>(
        best_sol); // Copy the best solution.

    return best_sol;
}

McDeval::Risk SingleObjectiveGeneticAlgorithm::get_best_solution_risk() const
{
    if (!best) {
        throw std::runtime_error("Cannot get best solution risk since the "
                                 "algorithm has not yet been run.");
    }

    McDeval::Risk risk = 0.0;
    eval_solution(*best, risk); // Evaluate best solution of the latest run.
    return risk;
}

void SingleObjectiveGeneticAlgorithm::create_evaluators()
{
    auto&& ogas = OpenGASingleton::get();
    if (alg_config.evaluators.size() == 1) {
        // Create a single evaluator with coefficient equal to 1 (which means
        // that coefficients are effectively disabled).
        ogas.evaluators.emplace_back(
            McDeval::Evaluator::create_evaluator(
                alg_config.evaluators[0].evaluator_type, app_config),
            1.0);
    } else {
        for (const EvaluatorData& ed : alg_config.evaluators) {
            ogas.evaluators.emplace_back(McDeval::Evaluator::create_evaluator(
                                             ed.evaluator_type, app_config),
                                         ed.coefficient.value());
        }
    }
}

namespace {

void init_genes(McDeval::Solution& s,
                const std::function<double(void)>& /*rnd01*/)
{
    // Initialize solution with random mapping of fragments and components.
    auto&& ogas = OpenGASingleton::get();
    s           = Random::solution(*ogas.app_config_p);
}

bool eval_solution(const McDeval::Solution& sol, McDeval::Risk& risk)
{
    auto&& ogas = OpenGASingleton::get();
    risk        = 0.0;

    // Always assume linear combination since if single evaluator is used,
    // coefficient is set to 1.0 (effectively disabled).
    for (auto&& ecp : ogas.evaluators) { // Evaluator and coefficient pair.
        auto& evaluator    = *ecp.first;
        double coefficient = ecp.second;
        risk += evaluator.evaluate(sol) * coefficient;
    }
    return true;
}

McDeval::Solution mutate(const McDeval::Solution& s,
                         const std::function<double(void)>& /*rnd01*/,
                         double /*shrink_scale*/)
{
    auto&& ogas = OpenGASingleton::get();
    if (Random::boolean()) {
        // Create a random solution.
        return Random::solution(*ogas.app_config_p);
    }
    McDeval::Solution sol(s);
    // Random server where assignable object will be reassigned.
    const McDeval::Server& server = Random::server(*ogas.app_config_p);

    bool fragment = Random::boolean();
    if (fragment) { // Random fragment should be reassigned.
        const McDeval::Fragment& fragment =
            Random::fragment(*ogas.app_config_p);
        sol.set_server(fragment, server);
    } else { // Random component should be reassigned.
        const McDeval::Component& component =
            Random::component(*ogas.app_config_p);
        sol.set_server(component, server);
    }

    return sol;
}

McDeval::Solution crossover(const McDeval::Solution& s1,
                            const McDeval::Solution& s2,
                            const std::function<double(void)>& /*rnd01*/)
{
    auto&& ogas = OpenGASingleton::get();
    McDeval::Solution child;

    // Get each fragment server randomly from first or the second parent.
    for (auto&& f : ogas.app_config_p->fragments) {
        auto&& s = Random::boolean() ? s1.get_server(f) : s2.get_server(f);
        child.set_server(f, s);
    }

    // Get each component server randomly from first or the second parent.
    for (auto&& c : ogas.app_config_p->components) {
        auto&& s = Random::boolean() ? s1.get_server(c) : s2.get_server(c);
        child.set_server(c, s);
    }

    return child;
}
double calculate_total_fitness(
    const EA::Genetic<McDeval::Solution, McDeval::Risk>::thisChromosomeType& x)
{
    return x.middle_costs;
}

void report_generation(
    int generation_number,
    const EA::GenerationType<McDeval::Solution, McDeval::Risk>& /*last_gen*/,
    const McDeval::Solution& best_genes)
{
    auto&& ogas = OpenGASingleton::get();
    if (generation_number % ogas.log_frequency != 0) { return; }

    McDeval::Risk best_risk = 0.0;
    eval_solution(best_genes, best_risk);

    std::cout << "GENERATION: " << generation_number << "\nBest solution:\n"
              << best_genes << "Best fitness: " << best_risk << "\n\n";
}

}
}
