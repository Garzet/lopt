#include "algorithm/ppes/Evaluations.hpp"

namespace PPES {

PredatorPreyEvaluator::PredatorPreyEvaluator(
    McDeval::Evaluator::Type evaluator_type,
    const McDeval::Configuration& app_config)
    : Objective(true, McDeval::Evaluator::type_to_string(evaluator_type)),
      app_config(app_config),
      evaluator_type(evaluator_type),
      evaluator(
          McDeval::Evaluator::create_evaluator(evaluator_type, app_config))
{}

double PredatorPreyEvaluator::evaluate(const McDeval::Solution& solution) const
{
    McDeval::Risk risk = evaluator->evaluate(solution);
    return risk;
}

std::unique_ptr<Objective<McDeval::Solution>>
PredatorPreyEvaluator::clone() const
{
    // Make new predator prey evaluator with the same settings as this object
    // was made (same name and same application configuraion).
    return std::make_unique<PredatorPreyEvaluator>(evaluator_type, app_config);
}

}
