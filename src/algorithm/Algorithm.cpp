#include "algorithm/Algorithm.hpp"
#include "algorithm/ecf/SingleObjectiveGeneticAlgorithm.hpp"
#include "algorithm/openga/MultiObjectiveGeneticAlgorithm.hpp"
#include "algorithm/openga/SingleObjectiveGeneticAlgorithm.hpp"
#include "algorithm/ppes/PredatorPreyAlgorithm.hpp"

#include <sstream>

#include "Log.hpp"

BaseAlgorithm::BaseAlgorithm(const AlgorithmConfiguration& alg_config,
                             const McDeval::Configuration& app_config)
    : alg_config(alg_config), app_config(app_config)
{
    // Validates parameters that are common for all algorithm types.
    validate_parameters();
}

void BaseAlgorithm::validate_parameters() const
{
    if (alg_config.type.empty()) { // Algorithm type.
        throw std::runtime_error("Algorithm type not set to a valid value.");
    }

    if (!alg_config.generation_max.has_value()) { // Maximum generations.
        throw_missing_parameter_error("generation_max");
    }

    if (!alg_config.log_frequency.has_value()) { // Logging frequency.
        throw_missing_parameter_error("log_frequency");
    }

    if (alg_config.evaluators.empty()) { // At least one evaluator is provided.
        throw std::runtime_error("No evaluators provided.");
    }
}

void BaseAlgorithm::throw_missing_parameter_error(
    const std::string& parameter_name) const
{
    std::stringstream ss;
    ss << "Parameter " << parameter_name << " for algortihm " << alg_config.type
       << " not set.\n";
    throw std::runtime_error(ss.str());
}

SingleObjectiveAlgorithm::SingleObjectiveAlgorithm(
    const AlgorithmConfiguration& alg_config,
    const McDeval::Configuration& app_config)
    : BaseAlgorithm(alg_config, app_config)
{
    // Validates parameters that are common for single objective algorithms.
    validate_parameters();
}

std::unique_ptr<SingleObjectiveAlgorithm>
SingleObjectiveAlgorithm::create(const AlgorithmConfiguration& alg_config,
                                 const McDeval::Configuration& app_config)
{
    if (alg_config.type == "OpenGA::SOGA") {
        return std::make_unique<OpenGA::SingleObjectiveGeneticAlgorithm>(
            alg_config, app_config);
    }
    if (alg_config.type == "ECF::SOGA") {
        return std::make_unique<ECF::SingleObjectiveGeneticAlgorithm>(
            alg_config, app_config);
    }
    return nullptr;
}

void SingleObjectiveAlgorithm::validate_parameters() const
{
    if (!alg_config.mutation_rate.has_value()) { // Mutation rate.
        throw_missing_parameter_error("mutation_rate");
    }

    if (!alg_config.population_size.has_value()) { // Population size.
        throw_missing_parameter_error("population_size");
    }

    // If more than one evaluator is defined, make sure each has coefficient.
    if (alg_config.evaluators.size() > 1) {
        for (const EvaluatorData& ed : alg_config.evaluators) {
            if (!ed.coefficient.has_value()) {
                std::stringstream ss;
                ss << "When " << alg_config.type
                   << " is used with more than "
                      "one evaluator, a coefficient must be provided for each "
                      "evaluator since they are linearly combined.";
                throw std::runtime_error(ss.str());
            }
        }
    }
}

MultiObjectiveAlgorithm::MultiObjectiveAlgorithm(
    const AlgorithmConfiguration& alg_config,
    const McDeval::Configuration& app_config)
    : BaseAlgorithm(alg_config, app_config)
{
    // Validates parameters that are common for multi objective algorithms.
    validate_parameters();
}

std::unique_ptr<MultiObjectiveAlgorithm>
MultiObjectiveAlgorithm::create(const AlgorithmConfiguration& alg_config,
                                const McDeval::Configuration& app_config)
{
    if (alg_config.type == "OpenGA::MOGA") {
        return std::make_unique<OpenGA::MultiObjectiveGeneticAlgorithm>(
            alg_config, app_config);
    }
    if (alg_config.type == "PPES::PPES") {
        return std::make_unique<PPES::PredatorPreyAlgorithm>(alg_config,
                                                             app_config);
    }
    return nullptr;
}

void MultiObjectiveAlgorithm::validate_parameters() const
{
    if (!alg_config.plot_frequency.has_value()) { // Plot frequency.
        throw_missing_parameter_error("plot_frequency");
    }

    if (!alg_config.pause_after_plot.has_value()) { // Pause after plot.
        throw_missing_parameter_error("pause_after_plot");
    }

    if (alg_config.evaluators.size() < 2) {
        throw std::runtime_error("At least two evaluators must be provided for "
                                 "multi objective algorithms.");
    }
    if (!alg_config.evaluators[0].plot_range.has_value() ||
        !alg_config.evaluators[1].plot_range.has_value()) {
        throw std::runtime_error("No plot range provided for both evaluators.");
    }

    if (alg_config.evaluators.size() > 2) {
        Log::warn("More than two evaluators have been provided, but {} "
                  "currently supports only two objectives. Two first defined "
                  "evaluators are used.",
                  alg_config.type);
    }
}
