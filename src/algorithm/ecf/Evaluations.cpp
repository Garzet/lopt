#include "algorithm/ecf/Evaluations.hpp"

#include "algorithm/ecf/LayoutGenotype.hpp"
#include "Log.hpp"

namespace ECF {

FitnessP LayoutEvaluateOperator::evaluate(IndividualP individual)
{
    FitnessP fitness(new FitnessMin);
    LayoutGenotypeP gen =
        boost::dynamic_pointer_cast<LayoutGenotype>(individual->getGenotype());
    const auto& solution = gen->solution;
    McDeval::Risk risk   = evaluate(solution);

    fitness->setValue(risk);
    return fitness;
}

SingleEvaluator::SingleEvaluator(const McDeval::Configuration& app_config,
                                 const AlgorithmConfiguration& alg_config)
{
    if (alg_config.evaluators.empty()) {
        throw std::runtime_error("No evaluators provided in configuration "
                                 "file.");
    }

    if (alg_config.evaluators.size() > 1) {
        Log::warn(
            "Multiple evaluators defined in the configuration file, but only "
            "one (first one defined) is used for solution evaluaton. Note that "
            "coefficient for that evaluator (if provided) is ignored.");
    }

    evaluator = McDeval::Evaluator::create_evaluator(
        alg_config.evaluators[0].evaluator_type, app_config);
}

McDeval::Risk SingleEvaluator::evaluate(const McDeval::Solution& solution)
{
    return evaluator->evaluate(solution);
}

LinearCombination::LinearCombination(const McDeval::Configuration& app_config,
                                     const AlgorithmConfiguration& alg_config)
{
    // Make sure at least two evaluators are given for linear combination.
    if (alg_config.evaluators.size() < 2) {
        throw std::runtime_error("At least two evaluators have to be provided "
                                 "in order to use linear combination of "
                                 "evaluators.");
    }

    // Push coefficients to linear combination object and create evaluators.
    for (const EvaluatorData& ed : alg_config.evaluators) {
        if (!ed.coefficient.has_value()) {
            throw std::runtime_error("Coefficient has to be provided for each "
                                     "evaluator in order to use linear "
                                     "combination of evaluators.");
        }
        coefficients.push_back(ed.coefficient.value());
        evaluators.push_back(McDeval::Evaluator::create_evaluator(
            ed.evaluator_type, app_config));
    }
}

McDeval::Risk LinearCombination::evaluate(const McDeval::Solution& solution)
{
    McDeval::Risk r = 0.0;
    for (size_t i = 0; i < evaluators.size(); i++) {
        r += evaluators[i]->evaluate(solution) * coefficients[i];
    }
    return r;
}

}
