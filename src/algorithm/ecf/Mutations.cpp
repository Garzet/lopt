#include "algorithm/ecf/Mutations.hpp"

#include "Random.hpp"
#include "algorithm/ecf/LayoutGenotype.hpp"

namespace ECF {

LayoutMutationOperator::LayoutMutationOperator(
    const McDeval::Configuration& app_config)
    : app_config(app_config)
{}

bool LayoutMutationOperator::mutate(GenotypeP gene)
{
    LayoutGenotypeP g      = boost::dynamic_pointer_cast<LayoutGenotype>(gene);
    McDeval::Solution& sol = g->solution; // Solution to mutate.
    mutate(sol); // Pass solution to method implemented by extending mutators.

    return true;
}

RandomAssignableToRandomServer::RandomAssignableToRandomServer(
    const McDeval::Configuration& app_config)
    : LayoutMutationOperator(app_config)
{}

void RandomAssignableToRandomServer::mutate(McDeval::Solution& sol)
{
    // Random server where assignable object will be reassigned.
    const McDeval::Server& server = Random::server(app_config);

    bool fragment = Random::boolean();
    if (fragment) { // Random fragment should be reassigned.
        const McDeval::Fragment& fragment = Random::fragment(app_config);
        sol.set_server(fragment, server);
    } else { // Random component should be reassigned.
        const McDeval::Component& component = Random::component(app_config);
        sol.set_server(component, server);
    }
}

RandomSolution::RandomSolution(const McDeval::Configuration& app_config)
    : LayoutMutationOperator(app_config)
{}

void RandomSolution::mutate(McDeval::Solution& sol)
{
    sol = Random::solution(app_config);
}

}
