#include "algorithm/ecf/SingleObjectiveGeneticAlgorithm.hpp"

#include "algorithm/ecf/ConfigurationFile.hpp"
#include "algorithm/ecf/Evaluations.hpp"
#include "algorithm/ecf/LayoutGenotype.hpp"

namespace ECF {

SingleObjectiveGeneticAlgorithm::SingleObjectiveGeneticAlgorithm(
    const AlgorithmConfiguration& alg_config,
    const McDeval::Configuration& app_config)
    : SingleObjectiveAlgorithm(alg_config, app_config), state(new State)
{
    // Create evaluation operator and register it to the ECF state object.
    if (alg_config.evaluators.size() == 1) {
        // Only one evaluator has been provided in algorithm configuration, so
        // single evaluator object is created since it does not require any
        // coefficients (they are meaningless when only one evaluator is given).
        state->setEvalOp(new SingleEvaluator(app_config, alg_config)); // NOLINT
    } else {
        // Multiple evaluators have been provided in algorithm configuration, so
        // linear combination of those will be used.
        state->setEvalOp(
            new LinearCombination(app_config, alg_config)); // NOLINT
    }

    // Create a layout genotype and register it to the ECF state object.
    LayoutGenotypeP genotype =
        static_cast<LayoutGenotypeP>(new LayoutGenotype(app_config));
    state->addGenotype(genotype);

    // Arguments for ECF.
    const char* argv_for_ecf[2] = {"", "res/ecf_parameters.xml"}; // NOLINT

    // Create ECF configuration file from algorithm configuration object.
    write_ecf_configuration_file(argv_for_ecf[1], alg_config);

    // Initialize the ECF state object from the ECF configuration file.
    state->initialize(2, const_cast<char**>(argv_for_ecf)); // NOLINT
}

McDeval::Solution SingleObjectiveGeneticAlgorithm::run()
{
    state->run();

    IndividualP best = state->getHoF()->getBest().back();
    LayoutGenotypeP best_gen =
        boost::dynamic_pointer_cast<LayoutGenotype>(best->getGenotype());
    return best_gen->solution;
}

McDeval::Risk SingleObjectiveGeneticAlgorithm::get_best_solution_risk() const
{
    IndividualP best = state->getHoF()->getBest().back();
    if (!best) {
        throw std::runtime_error("Cannot get best solution risk since the "
                                 "algorithm has not yet been run.");
    }
    FitnessP f = state->getEvalOp()->evaluate(best);

    return f->getValue();
}

}
