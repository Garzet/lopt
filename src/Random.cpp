#include "Random.hpp"

#include "Configuration.hpp"

#include <memory>
#include <random>
#include <sstream>

namespace Random {

class RandSingleton {
  public:
    // Random devices for entropy.
    std::random_device dev;
    std::mt19937 rng;

    // Distributions for fetching the random indices.
    std::uniform_int_distribution<size_t> result_dist;
    std::uniform_int_distribution<size_t> component_dist;
    std::uniform_int_distribution<size_t> server_dist;

    // Distribution for generatig random boolean values.
    std::bernoulli_distribution bool_dist;

    RandSingleton() : rng(dev()) {}

    static RandSingleton& get()
    {
        static RandSingleton rs;
        return rs;
    }
};

const McDeval::Fragment& fragment(const McDeval::Configuration& app_config)
{
    std::uniform_int_distribution<size_t> fragment_dist(
        0, app_config.fragments.size() - 1);
    auto&& rs           = RandSingleton::get();
    auto fragment_index = fragment_dist(rs.rng);
    return app_config.fragments[fragment_index];
}

const McDeval::Result& result(const McDeval::Configuration& app_config)
{
    std::uniform_int_distribution<size_t> result_dist(
        0, app_config.results.size() - 1);
    auto&& rs         = RandSingleton::get();
    auto result_index = result_dist(rs.rng);
    return app_config.results[result_index];
}

const McDeval::Component& component(const McDeval::Configuration& app_config)
{
    std::uniform_int_distribution<size_t> component_dist(
        0, app_config.components.size() - 1);
    auto&& rs            = RandSingleton::get();
    auto component_index = component_dist(rs.rng);
    return app_config.components[component_index];
}

const McDeval::Server& server(const McDeval::Configuration& app_config)
{
    std::uniform_int_distribution<size_t> server_dist(
        0, app_config.servers.size() - 1);
    auto&& rs         = RandSingleton::get();
    auto server_index = server_dist(rs.rng);
    return app_config.servers[server_index];
}

const McDeval::Resource& resource(const McDeval::Configuration& app_config)
{
    auto&& rs          = RandSingleton::get();
    bool get_fragement = rs.bool_dist(rs.rng);
    if (get_fragement) { return fragment(app_config); }
    return result(app_config);
}

McDeval::Solution solution(const McDeval::Configuration& app_config)
{
    McDeval::Solution s;

    // Assign each fragment and each component to a random server.
    for (auto&& f : app_config.fragments) {
        s.set_server(f, Random::server(app_config));
    }
    for (auto&& c : app_config.components) {
        s.set_server(c, Random::server(app_config));
    }

    return s;
}

bool boolean()
{
    auto&& rs = RandSingleton::get();
    return rs.bool_dist(rs.rng);
}

}
