#include "PPES.hpp"

#include "RandomDoubleMutation.hpp"

namespace PPES {

template <>
Prey<std::array<double, 2>>::Prey() {
    std::array<DoubleLimit, 2> limits = { DoubleLimit(0.3162, 1.0   ),
                                          DoubleLimit(   0.0, 2.2361) };
    if (limits.size() == 0) {
        throw std::runtime_error("Cannot create prey with zero dimensions.");
    }
    for (size_t i = 0, n = limits.size(); i < n; i++) {
        x[i] = Random::get_random_double(limits[i].lower, limits[i].upper);
    }
}

}
