#include "RandomDoubleMutation.hpp"

RandomDoubleMutation::RandomDoubleMutation() : Mutation("M1") {}

void RandomDoubleMutation::mutate(const std::array<double, 2>&,
                                  std::array<double, 2>& dest) const
{
    for (size_t i = 0, n = dest.size(); i < n; i++) {
        double lower = limits[i].lower;
        double upper = limits[i].upper;
        dest[i]      = PPES::Random::get_random_double(lower, upper);
    }
}
