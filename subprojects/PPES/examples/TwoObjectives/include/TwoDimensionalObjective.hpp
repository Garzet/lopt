#ifndef TWO_DIMENSIONAL_OBJECTIVE_HPP
#define TWO_DIMENSIONAL_OBJECTIVE_HPP

#include <PPES.hpp>

class F1 : public PPES::Objective<std::array<double, 2>> {
  private:
    const size_t n_variables;

  public:
    F1();

  private:
    double evaluate(const std::array<double, 2>& x) const override;

    std::unique_ptr<PPES::Objective<std::array<double, 2>>>
    clone() const override;
};

class F2 : public PPES::Objective<std::array<double, 2>> {
  private:
    const size_t n_variables;

  public:
    F2();

  private:
    double evaluate(const std::array<double, 2>& x) const override;

    std::unique_ptr<PPES::Objective<std::array<double, 2>>>
    clone() const override;
};

#endif
