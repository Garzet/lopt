#ifndef RANDOM_DOUBLE_MUTATION_HPP
#define RANDOM_DOUBLE_MUTATION_HPP

#include <PPES.hpp>

struct DoubleLimit {
    double lower;
    double upper;
    DoubleLimit(double lower, double upper) : lower(lower), upper(upper) {}
};

class RandomDoubleMutation : public PPES::Mutation<std::array<double, 2>> {
  private:
    std::array<DoubleLimit, 2> limits = {DoubleLimit(0.3162, 1.0),
                                         DoubleLimit(0.0, 2.2361)};

  public:
    RandomDoubleMutation();

  public:
    /** Performs a random mutation by setting the target values to random
     * number within the provided limits.
     * @param src Source gene to perform a mutation on. Ignored in this case
     *            since target is simply set to generated values.
     * @param dest Target gene to modify (set) as a result of mutation.
     * */
    virtual void mutate(const std::array<double, 2>& src,
                        std::array<double, 2>& dest) const override;
};

#endif
