#ifndef LOADER_HPP
#define LOADER_HPP

#include <inireader/INIReader.hpp>
#include <PPES.hpp>

#include <string>
#include <vector>

class Loader {
  private:
    INIReader r;
    std::string filename;

  public:
    Loader(const std::string& filename);

    size_t world_width() const;
    size_t world_height() const;
    size_t n_predators_per_objective() const;
    size_t log_frequency() const;
    size_t max_iterations() const;
    size_t plot_frequency() const;
    unsigned log_precision() const;
    bool pause_after_plot() const;
    std::string limits() const;
    std::string plot_range() const;
    std::string objectives() const;
    std::string mutations() const;

    PPES::Configuration create_configuration();

  private:
    std::vector<std::pair<double, double>>
    parse_plot_range(const std::string& range_str);

  private:
    /* Default values of attributes. */
    static constexpr size_t def_world_width     = static_cast<size_t>(5);
    static constexpr size_t def_world_height    = static_cast<size_t>(4);
    static constexpr char def_limits[]          = "0.3162 1 0 2.2361";
    static constexpr char def_objectives[]      = "F1 F2";
    static constexpr char def_mutations[]       = "M1";
    static constexpr char def_plot_range[]      = "0 1 0 10";
    static constexpr size_t def_max_iterations  = static_cast<size_t>(200);
    static constexpr unsigned def_log_precision = static_cast<unsigned>(4);
    static constexpr size_t def_n_predators_per_objective =
        static_cast<size_t>(1);

    /* Section names as strings. */
    static constexpr char sec_world[]     = "world";
    static constexpr char sec_algorithm[] = "algorithm";
    static constexpr char sec_prey[]      = "prey";
    static constexpr char sec_predator[]  = "predator";
    static constexpr char sec_logging[]   = "logging";
    static constexpr char sec_plotting[]  = "plotting";

    /* Attribute names as strings. */
    static constexpr char att_world_width[]      = "width";
    static constexpr char att_world_height[]     = "height";
    static constexpr char att_limits[]           = "limits";
    static constexpr char att_objectives[]       = "objectives";
    static constexpr char att_mutations[]        = "mutations";
    static constexpr char att_max_iterations[]   = "max_iterations";
    static constexpr char att_log_frequency[]    = "frequency";
    static constexpr char att_log_precision[]    = "precision";
    static constexpr char att_plot_frequency[]   = "frequency";
    static constexpr char att_pause_after_plot[] = "pause_after_plot";
    static constexpr char att_plot_range[]       = "range";
    static constexpr char att_n_predators_per_objective[] =
        "number_of_predators_per_objective";
};

#endif
