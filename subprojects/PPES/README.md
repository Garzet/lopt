# Predator-Prey Evolution Strategy

A simple C++ header-only implementation of the Predator-Prey algorithm for
multi-objective optimization.

## Prerequisites

1. C++17 compiler

#### Optional
2. [Gnuplot](http://www.gnuplot.info) - for plotting algorithm progress.

**NOTE:** Plotting is currently only supported on Unix-like systems, but Windows
          support should be trivial to implement.

**NOTE:** To enable plotting, gnuplot has to be available in system
          [PATH](https://en.wikipedia.org/wiki/PATH_(variable)).

## Getting started

Clone the repository:
`git clone https://gitlab.com/Garzet/predator-prey-evolution-strategy PPES`.

To use the library, simply include `PPES.hpp` in your project. See `examples`
for more details.

## Building examples

1. Position inside the directory: `cd examples/<EXAMPLE_DIRECTORY>`.
2. Run `make` to build the example (run `make release` to build with
   optimizations enabled).
3. Edit the configuration file (`res/config.ini`) to suit your needs.
4. Run the program: `./<EXAMPLE_EXECUTABLE> res/config.ini`.

**NOTE:** This project is template library so any solution type can be used. The
          default configuraion uses two floating point numbers as soltion. Since
          this is a template library, treat the code in `src` folder as example
          of library usage.

## Authors

* **Rudolf Lovrenčić**

## License

This project is licensed under the MIT License.
