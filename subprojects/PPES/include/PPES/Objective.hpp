#ifndef PPES_OBJECTIVE_HPP
#define PPES_OBJECTIVE_HPP

#include <vector>
#include <string>
#include <memory>

namespace PPES {

template<typename SolutionT>
class Objective {
  public:
    const bool minimization;
    const std::string name;

  public:
    Objective(bool minimization, std::string name)
        : minimization(minimization), name(std::move(name))
    {}

    virtual ~Objective()                              = default;
    virtual std::unique_ptr<Objective> clone() const  = 0;
    virtual double evaluate(const SolutionT& x) const = 0;

    friend std::ostream& operator<<(std::ostream& s, const Objective& obj)
    {
        return s << obj.name;
    }
};

}

#endif
