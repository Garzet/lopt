#ifndef PPES_CONFIGURATION_HPP
#define PPES_CONFIGURATION_HPP

#include <vector>

namespace PPES {

class Configuration {
  public:
    size_t world_width;
    size_t world_height;
    size_t n_predators_per_objective;
    size_t max_iterations;
    size_t log_frequency;
    size_t plot_frequency;
    unsigned log_precision;
    bool pause_after_plot;
    std::vector<std::pair<double, double>> plot_range;

    Configuration(size_t world_width,
                  size_t world_height,
                  size_t n_predators_per_objective,
                  size_t max_iterations,
                  size_t log_frequency,
                  size_t plot_frequency,
                  unsigned log_precision,
                  bool pause_after_plot,
                  std::vector<std::pair<double, double>> plot_range)
        : world_width(world_width),
          world_height(world_height),
          n_predators_per_objective(n_predators_per_objective),
          max_iterations(max_iterations),
          log_frequency(log_frequency),
          plot_frequency(plot_frequency),
          log_precision(log_precision),
          pause_after_plot(pause_after_plot),
          plot_range(std::move(plot_range))
    {}
};

}

#endif
