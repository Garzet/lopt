#ifndef PPES_PLOTTER_HPP
#define PPES_PLOTTER_HPP

#include "Log.hpp"
#include "Configuration.hpp"
#include "Objective.hpp"

#include "Gnuplot.hpp"

namespace PPES {

template<typename SolutionT>
class Algorithm;

template<typename SolutionT>
class Plotter {
  private:
    const Configuration& c;
    const std::vector<std::unique_ptr<Objective<SolutionT>>>& objs;
    bool enabled;
    std::unique_ptr<Gnuplot> gp;
    const bool pause_after_plot;

  public:
    Plotter(const Configuration& c,
            const std::vector<std::unique_ptr<Objective<SolutionT>>>& objs);
    void plot(const Algorithm<SolutionT>& a);
};

template<typename SolutionT>
Plotter<SolutionT>::Plotter(
    const Configuration& c,
    const std::vector<std::unique_ptr<Objective<SolutionT>>>& objs)
    : c(c),
      objs(objs),
      enabled(objs.size() == 2 && c.plot_frequency < c.max_iterations),
      gp(nullptr),
      pause_after_plot(c.pause_after_plot)
{
    if (!enabled) { return; } // Check if plotting is enabled.

    // Try to create gnuplot object. Checks weather gnuplot is installed, tries
    // to open a gnuplot process and open a pipe to it. If any part of that
    // process fails, plotting is disabled and warning is logged.
    try {
        gp = std::make_unique<Gnuplot>();
    } catch (const GnuplotException& exc) {
        enabled = false; // Disable plotting.
        Log::warn("Plotting is disabled: {}", exc.what());
        return;
    }

    auto x_range = c.plot_range[0];
    auto y_range = c.plot_range[1];

    *gp << "set grid\n"
        << "set style line 1 pointtype 1 pointsize 2\n"
        << "set xrange [" << std::to_string(x_range.first) << ':'
        << std::to_string(x_range.second) << "]\n"
        << "set yrange [" << std::to_string(y_range.first) << ':'
        << std::to_string(y_range.second) << "]\n";
}

template<typename SolutionT>
void Plotter<SolutionT>::plot(const Algorithm<SolutionT>& a)
{
    if (!enabled) { return; } // Check if plotting is enabled.

    std::vector<std::pair<double, double>> pts(a.world.preys.size());

    // Evaluate each prey for at each objective and put the value in stream.
    std::stringstream ss;
    for (auto&& prey : a.world.preys) {
        for (auto&& objective : objs) {
            ss << objective->evaluate(prey.get_data()) << ' ';
        }
        ss << '\n'; // All objectives for one prey have been evaluated.
    }
    ss << "e\n"; // Signal end of data to gnuplot.

    *gp << "plot '-' linestyle 1\n" // Gnuplot takes data from standard input.
        << ss.str();                // Send data to gnuplot.
    gp->flush();                    // Flush gnuplot pipe. Graph should appear.

    if (pause_after_plot) {
        Log::normal("Plot complete. Press ENTER to continue...");
        std::getchar();
    }
}

}

#endif
