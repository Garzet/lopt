#ifndef PPES_PREY_HPP
#define PPES_PREY_HPP

#include <vector>
#include <iostream>

#include "Mutation.hpp"

namespace PPES {

template<typename SolutionT>
class Prey {
  protected:
    SolutionT x;

  public:
    /** Creates a prey by constructing a solution with default constructor.
     * A template specialization for this function has to be provieded for the
     * given solution type. */
    Prey() = default;

    /** Creates a prey (solution) with provided values.
     * @param values Initial prey values. */
    explicit Prey(SolutionT values);

    /** Mutates the provided pray and replaces this pray with mutation result.
     * @param src Pray to mutate and use to replace this pray.
     * @param m Mutation to apply to source to create a new prey which replaces
     *          this prey. */
    void replace_with_mutated(const Prey& src, const Mutation<SolutionT>& m);

    inline const SolutionT& get_data() const { return x; }

    template<typename T>
    friend std::ostream& operator<<(std::ostream& s, const Prey<T>& p);
};

template<typename SolutionT>
Prey<SolutionT>::Prey(SolutionT values) : x(std::move(values))
{}

template<typename SolutionT>
void Prey<SolutionT>::replace_with_mutated(const Prey& src,
                                           const Mutation<SolutionT>& m)
{
    m.mutate(src.get_data(), x);
}

template<typename T>
std::ostream& operator<<(std::ostream& s, const Prey<T>& p)
{
    return s << p.x;
}

}

#endif
