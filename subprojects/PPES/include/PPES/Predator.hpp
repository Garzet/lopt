#ifndef PPES_PREDATOR_HPP
#define PPES_PREDATOR_HPP

#include <algorithm>

#include "Objective.hpp"
#include "Coordinate.hpp"
#include "Direction.hpp"
#include "Prey.hpp"
#include "Random.hpp"
#include "HaloArray.hpp"

namespace PPES {

template<typename SolutionT>
class World;

template<typename SolutionT>
class Predator {
  private:
    std::unique_ptr<Objective<SolutionT>> objective;

  public:
    Coordinate coordinate;

  public:
    /** Creates a predator with the provided objective function at the random
     * starting position. Note that this transfers ownership of Objective object
     * to constructed Predator object.
     * @param o Objective function used by this predator.
     * @param world_w World width.
     * @param world_h World height. */
    Predator(std::unique_ptr<Objective<SolutionT>> o,
             size_t world_w,
             size_t world_h);

    /** Creates a predator with the provided objective function at the provided
     * world coordinate. Note that this transfers ownership of Objective object
     * to constructed Predator object.
     * @param o Objective function used by this predator.
     * @param c Initial coordinate of the predator. */
    Predator(std::unique_ptr<Objective<SolutionT>> o, Coordinate c);

    /** Returns direction of the wanted prey. Direction is determined based on
     * the prey in the provided array.
     * @param ps Preys surrounding the given coordinate predator. First element
     *           in the array points to prey North(N) of predator and other
     *           follow in counter clockwise order (second element is
     *           North-West(NW)).
     * @return Direction of the wanted prey. */
    Direction evaluate_prey(const HaloArray<const Prey<SolutionT>*>& ps) const;

    /** Predator walks in the random direction. */
    void walk(const World<SolutionT>& w);

    template<typename T>
    friend std::ostream& operator<<(std::ostream& s, const Predator<T>& p);
};

template<typename SolutionT>
Predator<SolutionT>::Predator(std::unique_ptr<Objective<SolutionT>> o,
                              size_t world_w,
                              size_t world_h)
    : objective(std::move(o)),
      coordinate(Random::get_random_int(0, world_h - 1), // Random row.
                 Random::get_random_int(0, world_w - 1))
{} // Random column.

template<typename SolutionT>
Predator<SolutionT>::Predator(std::unique_ptr<Objective<SolutionT>> o,
                              Coordinate c)
    : objective(std::move(o)), coordinate(c)
{}

template<typename SolutionT>
Direction Predator<SolutionT>::evaluate_prey(
    const HaloArray<const Prey<SolutionT>*>& ps) const
{
    // Compare preys using the objective function of this predator.
    auto comp_func = [this](const Prey<SolutionT>* p1,
                            const Prey<SolutionT>* p2) {
        return objective->evaluate(p1->get_data()) <
               objective->evaluate(p2->get_data());
    };

    auto wpi = objective->minimization ? // Goal is to minimize.
                   std::max_element(ps.begin(),
                                    ps.end(),
                                    comp_func) : // Goal is to maximize.
                   std::min_element(ps.begin(), ps.end(), comp_func);

    // Get direction based on index (iterator distance from beginning).
    auto d = static_cast<Direction>(std::distance(ps.begin(), wpi));
    return d;
}

template<typename SolutionT>
void Predator<SolutionT>::walk(const World<SolutionT>& w)
{
    // Pick a random direction, get the coordinate in that direction and set it.
    auto d =
        static_cast<Direction>(Random::get_random_int(0, n_directions - 1));
    coordinate = w.get_coordinate(coordinate, d);
}

template<typename T>
std::ostream& operator<<(std::ostream& s, const Predator<T>& p)
{
    return s << '[' << p.coordinate << ',' << *p.objective << ']';
}

}

#endif
