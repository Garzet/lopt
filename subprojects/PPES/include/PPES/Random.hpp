#ifndef PPES_RANDOM_HPP
#define PPES_RANDOM_HPP

#include <cstdlib>
#include <random>
#include <stdexcept>

namespace PPES::Random {

/* Produces a true random number. Used to seed a pseudo-random number generator
 * since pseudo-random number generator offers better performance and cannot
 * exhaust if many subsequent calls are made. */
inline std::random_device dev;                      // NOLINT
inline std::default_random_engine generator(dev()); // NOLINT

/** Returns a random double value contained within the limits.
 * @param lower_limit Lower bound of the double value (inclusive).
 * @param upper_limit Upper bound of the double value (exclusive).
 * @return Double value contained within the provided limits. */
inline double get_random_double(double lower_limit, double upper_limit)
{
    if (lower_limit > upper_limit) {
        throw std::runtime_error("Cannot generate random number when lower"
                                 "limit is larger than upper limit.");
    }
    std::uniform_real_distribution<double> d(lower_limit, upper_limit);
    return d(generator);
}

/** Returns a random integer value contained within the limits.
 * @param lower_limit Lower bound of the integer value (inclusive).
 * @param upper_limit Upper bound of the double value (inclusive).
 * @return Integer value contained within the provided limits. */
inline size_t get_random_int(size_t lower_limit, size_t upper_limit)
{
    if (lower_limit > upper_limit) {
        throw std::runtime_error("Cannot generate random number when lower"
                                 "limit is larger than upper limit.");
    }
    std::uniform_int_distribution<size_t> d(lower_limit, upper_limit);
    return d(generator);
}

/** Sets the seed of a pseudo-random number generator so reproducible results
 * can be achieved. Do not call this function if you do not need reproducible
 * results since pseudo-random generator is automatically seeded with truly
 * random number generator when the program starts.
 * @param seed A seed for pseudo-random number generator. */
inline void set_seed(unsigned seed)
{
    generator.seed(seed);
}

}

#endif
