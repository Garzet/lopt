#ifndef PPES_HALOARRAY_HPP
#define PPES_HALOARRAY_HPP

#include <array>

namespace PPES {

/* Number of directions in the PPES world. */
constexpr std::size_t n_directions = 8;

/* Array that holds objects for each of possible direction in the PPES world. */
template<typename T>
using HaloArray = std::array<T, n_directions>;

}

#endif
