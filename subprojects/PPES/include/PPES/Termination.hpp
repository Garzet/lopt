#ifndef PPES_TERMINATION_HPP
#define PPES_TERMINATION_HPP

#include "Configuration.hpp"

#include <ostream>
#include <memory>

namespace PPES {

template<typename SolutionT>
class Algorithm;

template<typename SolutionT>
class TerminationCriteria {
  public:
    virtual ~TerminationCriteria() = default;
    virtual bool is_met() const    = 0;

    static std::vector<std::unique_ptr<TerminationCriteria>>
    get_termination_criteria(const Configuration& c,
                             const Algorithm<SolutionT>& a);

    template<typename T>
    friend std::ostream& operator<<(std::ostream& s,
                                    const TerminationCriteria<T>& tc);

  private:
    virtual std::ostream& to_stream(std::ostream& s) const = 0;
};

template<typename SolutionT>
class MaxIterations : public TerminationCriteria<SolutionT> {
  private:
    unsigned long long max_iter;
    const Algorithm<SolutionT>& a;

  public:
    MaxIterations(unsigned long long max_iter, const Algorithm<SolutionT>& a);
    bool is_met() const override;

  private:
    std::ostream& to_stream(std::ostream& s) const override;
};

template<typename SolutionT>
std::vector<std::unique_ptr<TerminationCriteria<SolutionT>>>
TerminationCriteria<SolutionT>::get_termination_criteria(
    const Configuration& c, const Algorithm<SolutionT>& a)
{
    std::vector<std::unique_ptr<TerminationCriteria>> ret;
    {
        std::unique_ptr<MaxIterations<SolutionT>> p =
            std::make_unique<MaxIterations<SolutionT>>(c.max_iterations, a);
        ret.emplace_back(std::move(p));
    }
    return ret;
}

template<typename SolutionT>
std::ostream& operator<<(std::ostream& s,
                         const TerminationCriteria<SolutionT>& tc)
{
    return tc.to_stream(s);
}

template<typename SolutionT>
MaxIterations<SolutionT>::MaxIterations(unsigned long long max_iter,
                                        const Algorithm<SolutionT>& a)
    : max_iter(max_iter), a(a)
{}

template<typename SolutionT>
bool MaxIterations<SolutionT>::MaxIterations::is_met() const
{
    return a.get_current_iteration() >= max_iter;
}

template<typename SolutionT>
std::ostream& MaxIterations<SolutionT>::to_stream(std::ostream& s) const
{
    return s << "Maximum number of iterations reached.";
}

}

#endif
