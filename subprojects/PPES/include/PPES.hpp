#ifndef PPES_HPP
#define PPES_HPP

#include "PPES/Algorithm.hpp"
#include "PPES/Log.hpp"
#include "PPES/Prey.hpp"
#include "PPES/Random.hpp"
#include "PPES/Mutation.hpp"
#include "PPES/Objective.hpp"

#endif
