#ifndef MCDEVAL_EVALUATOR_HPP
#define MCDEVAL_EVALUATOR_HPP

#include "McDeval/Solution.hpp"

#include <memory>

namespace McDeval {

using Risk = double;

/** Base class for all evaluators. */
class Evaluator {
  protected:
    const Configuration& config;

  public:
    explicit Evaluator(const Configuration& config);
    virtual ~Evaluator() = default;

    virtual Risk evaluate(const Solution& solution) = 0;

    /** Evaluator type enumberation. Used for creating an evaluator by using a
     * static factory function. */
    enum class Type { Constraint, Resident, SingleResource };

    /** Converts evaluator type to a name. */
    static std::string type_to_string(Type evaluator_type);

    /** Creates an evaluator instance based on the given evaluator type. */
    static std::unique_ptr<Evaluator>
    create_evaluator(Type evaluator_type, const Configuration& config);
};

/** Evaluator that punishes broken security constraints.  */
class ConstraintEvaluator : public Evaluator {
  private:
    double punishment;

  public:
    explicit ConstraintEvaluator(const Configuration& config,
                                 double punishment = 100.0);
    Risk evaluate(const Solution& solution) override;
};

/** Evaluator that punishes if fragment and component reside on the same
 * server. */
class ResidentEvaluator : public Evaluator {
  private:
    double punishment;

  public:
    explicit ResidentEvaluator(const Configuration& config,
                               double punishment = 100.0);
    Risk evaluate(const Solution& solution) override;
};

/** Evaluator that punishes more if the more important fragment is placed on
 * less trusted server. */
class SingleResourceEvaluator : public Evaluator {
  public:
    explicit SingleResourceEvaluator(const Configuration& config);
    Risk evaluate(const Solution& solution) override;
};

}

#endif
