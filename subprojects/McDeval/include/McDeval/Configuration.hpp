#ifndef MCDEVAL_CONFIGURATION_HPP
#define MCDEVAL_CONFIGURATION_HPP

#include <vector>
#include <memory>
#include <optional>

#include "McDeval/Constraint.hpp"
#include "McDeval/Fragment.hpp"
#include "McDeval/Component.hpp"
#include "McDeval/Server.hpp"
#include "McDeval/Result.hpp"

namespace McDeval {

class Configuration {
  public:
    std::vector<Fragment> fragments;
    std::vector<Result> results;
    std::vector<Component> components;
    std::vector<Server> servers;
    std::vector<Constraint> constraints;

  public:
    /** Validates configuration - looks for errors in configuration. */
    void validate() const;

    /** Gets component by name from the provided configuration object. Returns
     * nullptr if no component with provided name exists in the provided
     * configuration object. */
    const Component* get_component(const std::string& name) const;

    /** Gets fragment by name from the provided configuration object. Returns
     * nullptr if no fragment with provided name exists in the provided
     * configuration object. */
    const Fragment* get_fragment(const std::string& name) const;

    /** Gets server by name from the provided configuration object. Returns
     * nullptr if no server with the provided name exists in the provided
     * configuration object. */
    const Server* get_server(const std::string& name) const;

    friend std::ostream& operator<<(std::ostream& s,
                                    const Configuration& app_config);

  private:
    /** Makes sure that each result is output of one and only one component.
     * This functions throws an error if the result has no source or has
     * multiple sources. */
    void unknown_result_source_check() const;

    /** Makes sure that IDs in each collections are unique. Furthermore, the
     * function checks that all resource IDs are unique between their
     * collections because they are all referenced in the input and output
     * list of components by their IDs. This function throws an error if it
     * finds any duplicate IDs. Internally, the function uses hashing to check
     * if IDs are unique, so it assumes that hash of objects is calculated only
     * based on their IDs. */
    void duplicate_id_check() const;

    /** Makes sure that no two constraints are defined on exactly the same
     * resources. */
    void constraints_with_same_resources_check() const;

    /** Makes sure that each fragment in the configuration vector has at least
     * one destination component set. Furthermore, the function checks that
     * destinations of all fragments are consistent with pointers in each
     * component since there are pointers in both directions - component points
     * to accessed fragments and fragment points to its destination components.
     * This function throws an error if at least one destination is not set for
     * each fragment or is not consistent with components pointers. */
    void fragments_destination_check() const;

    /** Makes sure that each results in the configuration vector has its source
     * and destination component set. Furthermore, the function checks that
     * sources and destinations of all results are consistent with pointers in
     * each component since there are pointers in both directions - component
     * points to results it uses and produces and result points to its source
     * and destination components. This function throws an error if source or
     * destination is not set for any result or is not consistent with
     * components pointers. */
    void results_source_and_destination_check() const;
};

}
#endif
