#ifndef MCDEVAL_FRAGMENT_HPP
#define MCDEVAL_FRAGMENT_HPP

#include "McDeval/Resource.hpp"

namespace McDeval {

class Fragment : public Resource {
  private:
    /** Components that access this data fragment. All pointers are non-owning
     * and are guaranteed to not be nullptr. Pointers point to objects in
     * configuration object so for during the lifetime of this object it should
     * be guaranteed that part of configuration object that stores fragments
     * will not change. */
    std::vector<const Component*> destinations;

  public:
    Fragment(const std::string& id, unsigned importance);

    void add_destination(const Component& component);

    bool destination_set() const;

    ServerSet server_reach(const Solution& s) const override;

    inline std::vector<const Component*>::const_iterator
    destinations_begin() const
    {
        return destinations.begin();
    }

    inline std::vector<const Component*>::const_iterator
    destinations_end() const
    {
        return destinations.end();
    }
};

}

#endif
