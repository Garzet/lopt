#ifndef MCDEVAL_HASHER_HPP
#define MCDEVAL_HASHER_HPP

#include <functional>

namespace McDeval {
class Constraint;
class Component;
class Server;
class Solution;
class Resource;
}

namespace std {

template<>
struct hash<McDeval::Constraint> {
    size_t operator()(const McDeval::Constraint& c) const;
};

template<>
struct hash<McDeval::Component> {
    size_t operator()(const McDeval::Component& c) const;
};

template<>
struct hash<McDeval::Server> {
    size_t operator()(const McDeval::Server& s) const;
};

template<>
struct hash<McDeval::Resource> {
    size_t operator()(const McDeval::Resource& r) const;
};

template<>
struct hash<McDeval::Solution> {
    size_t operator()(const McDeval::Solution& s) const;
};

}

#endif
