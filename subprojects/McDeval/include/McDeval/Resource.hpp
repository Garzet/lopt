#ifndef MCDEVAL_RESOURCE_HPP
#define MCDEVAL_RESOURCE_HPP

#include "McDeval/ServerSet.hpp"

namespace McDeval {

class Solution;

class Resource {
  public:
    /** Unique resource identifier. */
    const std::string id;

    /** Importance of the resource. If this value is higher, more damage is
     * caused when this fragment has been compromised (leaked). */
    const unsigned importance;

  public:
    Resource(std::string id, unsigned importance);
    virtual ~Resource() = default;

    /** Calculates a set of servers on which this resource is available based on
     * the current application layout (current solution). */
    virtual ServerSet server_reach(const Solution& s) const = 0;

    unsigned get_importance() const;

    bool operator==(const Resource& other) const;
    bool operator==(const std::string& other_id) const;
    bool operator!=(const Resource& other) const;
    bool operator!=(const std::string& other_id) const;

    friend std::ostream& operator<<(std::ostream& s, const Resource& r);

    friend size_t std::hash<Resource>::operator()(const Resource& r) const;
};

}

#endif
