option('example',
       type : 'combo',
       choices : ['None', 'Visualizer'],
       value : 'None',
       description: 'Example to build.')

option('tests',
       type: 'boolean',
       value: false,
       description: 'Build tests.')
