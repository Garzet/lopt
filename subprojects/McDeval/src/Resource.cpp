#include "McDeval/Resource.hpp"

#include <functional>

namespace McDeval {

Resource::Resource(std::string id, unsigned importance)
    : id(std::move(id)), importance(importance)
{}

unsigned Resource::get_importance() const { return importance; }

bool Resource::operator==(const Resource& other) const
{
    return id == other.id;
}

bool Resource::operator==(const std::string& other_id) const
{
    return id == other_id;
}

bool Resource::operator!=(const Resource& other) const
{
    return !(*this == other);
}

bool Resource::operator!=(const std::string& other_id) const
{
    return !(*this == other_id);
}

std::ostream& operator<<(std::ostream& s, const Resource& r)
{
    return s << r.id;
}

}
