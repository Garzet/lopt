#include "McDeval/Constraint.hpp"

#include "McDeval/Solution.hpp"

#include <sstream>

namespace McDeval {

Constraint::Constraint(std::string id,
                       const std::vector<const Fragment*>& fragments,
                       const std::vector<const Result*>& results)
    : id(std::move(id)), fragments(fragments), results(results)
{
    if (fragments.size() + results.size() < 2) {
        std::stringstream ss;
        ss << "Constraint " << *this << " defined on less than two resources.";
        throw std::runtime_error(ss.str());
    }

    // Make sure that constraint does not contain duplicate fragments.
    for (size_t i = 0; i < fragments.size(); i++) {
        for (size_t j = i + 1; j < fragments.size(); j++) {
            const Fragment& f1 = *fragments[i];
            const Fragment& f2 = *fragments[j];
            if (f1 == f2) {
                std::stringstream s;
                s << "Constraint " << *this << " contains duplicate fragments.";
                throw std::runtime_error(s.str());
            }
        }
    }

    // Make sure that constraint does not contain duplicate results.
    for (size_t i = 0; i < results.size(); i++) {
        for (size_t j = i + 1; j < results.size(); j++) {
            const Result& r1 = *results[i];
            const Result& r2 = *results[j];
            if (r1 == r2) {
                std::stringstream ss;
                ss << "Constraint " << *this << " contains duplicate results.";
                throw std::runtime_error(ss.str());
            }
        }
    }
}

ServerSet Constraint::get_breaking_servers(const Solution& sol) const
{
    // Get server reach of each resource. Intersection of those reaches is a set
    // of breaking servers. Return only one of those servers.

    // If there are no fragments in the constraint, starting inteseaction of
    // reaches is reach of first result.
    ServerSet intersect = fragments.empty() ? results[0]->server_reach(sol) :
                                              fragments[0]->server_reach(sol);

    // Calculate intersection with all fragment server reaches.
    for (const Fragment* f : fragments) {
        ServerSet sr = f->server_reach(sol);
        intersect.inplace_intersect(sr);
    }

    // Calculate intersection with all result server reaches.
    for (const Result* r : results) {
        ServerSet sr = r->server_reach(sol);
        intersect.inplace_intersect(sr);
    }

    return intersect;
}

bool Constraint::is_fulfiled(const Solution& solution) const
{
    return get_breaking_servers(solution).empty();
}

bool Constraint::is_broken(const Solution& solution) const
{
    return !is_fulfiled(solution);
}

bool Constraint::operator==(const Constraint& other) const
{
    return id == other.id;
}

bool Constraint::operator!=(const Constraint& other) const
{
    return !(*this == other);
}

bool Constraint::contains_same_resources_as(const Constraint& other) const
{
    // Check that number of resources is the same in both objects.
    if (fragments.size() != other.fragments.size() ||
        results.size() != other.results.size()) {
        return false;
    }

    // Check that each fragment in this object is also in other.
    auto&& of = other.fragments; // Shortcut reference.
    for (auto&& f : fragments) { // Fragment of this object.
        // Check if fragment of this objects can be found in other.
        if (std::find(of.begin(), of.end(), f) == of.end()) {
            return false; // Fragment not found in other.
        }
    }

    // Check that each result in this object is also in other.
    auto&& res = other.results; // Shortcut reference.
    for (auto&& r : results) {  // Result of this object.
        // Check if result of this objects can be found in other.
        if (std::find(res.begin(), res.end(), r) == res.end()) {
            return false; // Fragment not found in other.
        }
    }

    return true; // All resources of this object have been found in other.
}

std::ostream& operator<<(std::ostream& s, const Constraint& c)
{
    return s << c.id;
}

}
