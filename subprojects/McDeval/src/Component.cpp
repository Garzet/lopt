#include "McDeval/Component.hpp"

namespace McDeval {

Component::Component(std::string id,
                     const std::vector<const Resource*>& inputs,
                     const std::vector<const Result*>& outputs)
    : id(std::move(id)), inputs(inputs), outputs(outputs)
{
    // Check if given inputs or outputs hold null pointer.
    if (std::find(inputs.begin(), inputs.end(), nullptr) != inputs.end()) {
        throw std::runtime_error("Component input vector cannot hold null "
                                 "pointers.");
    }
    if (std::find(outputs.begin(), outputs.end(), nullptr) != outputs.end()) {
        throw std::runtime_error("Component output vector cannot hold null "
                                 "pointers.");
    }
}

std::ostream& operator<<(std::ostream& s, const Component& c)
{
    return s << c.id;
}

std::vector<const Resource*>::const_iterator Component::inputs_begin() const
{
    return inputs.begin();
}

std::vector<const Resource*>::const_iterator Component::inputs_end() const
{
    return inputs.end();
}

std::vector<const Result*>::const_iterator Component::outputs_begin() const
{
    return outputs.begin();
}

std::vector<const Result*>::const_iterator Component::outputs_end() const
{
    return outputs.end();
}

bool Component::operator==(const Component& other) const
{
    return id == other.id;
}

bool Component::operator==(const std::string& other) const
{
    return id == other;
}

}
