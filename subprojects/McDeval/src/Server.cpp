#include "McDeval/Server.hpp"

namespace McDeval {

Server::Server(std::string id, unsigned trust) : id(std::move(id)), trust(trust)
{}

unsigned Server::get_trust() const
{
    return trust;
}

bool Server::operator==(const Server& other) const
{
    return id == other.id;
}

bool Server::operator==(const std::string& other) const
{
    return id == other;
}

bool Server::operator!=(const Server& other) const
{
    return !(*this == other);
}

bool Server::operator<(const Server& other) const
{
    return id < other.id;
}

std::ostream& operator<<(std::ostream& s, const Server& ser)
{
    s << ser.id;
    return s;
}

}
