#include "McDeval/Configuration.hpp"

#include <unordered_set>
#include <sstream>

namespace McDeval {

void Configuration::validate() const
{
    duplicate_id_check();
    unknown_result_source_check();
    fragments_destination_check();
    results_source_and_destination_check();
    constraints_with_same_resources_check();
}

const Component* Configuration::get_component(const std::string& name) const
{
    auto res_iter = std::find(components.begin(), components.end(), name);
    if (res_iter == components.end()) { return nullptr; }
    return &*res_iter;
}

const Fragment* Configuration::get_fragment(const std::string& name) const
{
    auto res_iter = std::find(fragments.begin(), fragments.end(), name);
    if (res_iter == fragments.end()) { return nullptr; }
    return &*res_iter;
}

const Server* Configuration::get_server(const std::string& name) const
{
    auto res_iter = std::find(servers.begin(), servers.end(), name);
    if (res_iter == servers.end()) { return nullptr; }
    return &*res_iter;
}

std::ostream& operator<<(std::ostream& s, const Configuration& app_config)
{
    return s << "Number of fragments:   " << app_config.fragments.size()
             << "\nNumber of results:     " << app_config.results.size()
             << "\nNumber of components:  " << app_config.components.size()
             << "\nNumber of servers:     " << app_config.servers.size()
             << "\nNumber of constraints: " << app_config.constraints.size();
}

void Configuration::unknown_result_source_check() const
{
    // For every result, find all sources. For this check to pass, there needs
    // to be one and only one source for each defined result.
    for (const Result& r : results) {
        const Component* result_source = nullptr; // Result source not found.

        // Iterate over all components.
        for (const Component& c : components) {
            // Iterate over all outputs (results) of the component.
            for (auto&& it = c.outputs_begin(); it != c.outputs_end(); ++it) {
                if (**it == r) {         // Found result source.
                    if (result_source) { // Second result source found.
                        std::stringstream ss;
                        ss << "Result " << r
                           << " is referenced as output by "
                              "multiple components.";
                        throw std::runtime_error(ss.str());
                    }
                    result_source = &c;
                }
            }
        }

        if (!result_source) { // No component found that output this result.
            std::stringstream ss;
            ss << "No components produce result " << r << " as their output.";
            throw std::runtime_error(ss.str());
        }
    }
}

void Configuration::duplicate_id_check() const
{
    // Check for duplicate resources (fragments and results). This is done with
    // regular loops since Resource is abstract class, so it is inconvenient to
    // check for duplicates by using sets.
    {
        // Duplicate fragments check.
        for (size_t i = 0; i < fragments.size(); i++) {
            for (size_t j = i + 1; j < fragments.size(); j++) {
                const Fragment& f1 = fragments[i];
                const Fragment& f2 = fragments[j];
                if (f1 == f2) {
                    std::stringstream ss;
                    ss << "Multiple fragments with ID equal to " << f1
                       << " have been found.";
                    throw std::runtime_error(ss.str());
                }
            }
        }

        // Duplicate results check.
        for (size_t i = 0; i < results.size(); i++) {
            for (size_t j = i + 1; j < results.size(); j++) {
                const Result& r1 = results[i];
                const Result& r2 = results[j];
                if (r1 == r2) {
                    std::stringstream ss;
                    ss << "Multiple results with ID equal to " << r1
                       << " have been found.";
                    throw std::runtime_error(ss.str());
                }
            }
        }

        // Duplicate resources check (between fragments and results).
        for (const Fragment& f : fragments) {
            for (const Result& r : results) {
                if (f == r) {
                    std::stringstream ss;
                    ss << "Fragment and result with ID equal to " << f
                       << " have been found.";
                    throw std::runtime_error(ss.str());
                }
            }
        }
    }

    { // Duplicate components check.
        std::unordered_set<Component> cs(components.begin(), components.end());
        if (cs.size() != components.size()) {
            throw std::runtime_error("Duplicate components found.");
        }
    }

    { // Duplicate servers check.
        std::unordered_set<Server> ss(servers.begin(), servers.end());
        if (ss.size() != servers.size()) {
            throw std::runtime_error("Duplicate servers found.");
        }
    }

    { // Duplicate constraints check.
        std::unordered_set<Constraint> cs(constraints.begin(),
                                          constraints.end());
        if (cs.size() != constraints.size()) {
            throw std::runtime_error("Duplicate constraints found.");
        }
    }
}

void Configuration::constraints_with_same_resources_check() const
{
    for (size_t i = 0; i < constraints.size(); i++) {
        for (size_t j = i + 1; j < constraints.size(); j++) {
            auto&& c1 = constraints[i]; // Shortcut reference.
            auto&& c2 = constraints[j]; // Shortcut reference.
            if (c1.contains_same_resources_as(c2)) {
                std::stringstream ss;
                ss << "Constraints " << c1 << " and " << c2
                   << " are defined "
                      "on exactly the same resources.";
                throw std::runtime_error(ss.str());
            }
        }
    }
}

void Configuration::fragments_destination_check() const
{
    for (const Fragment& f : fragments) {
        // Make sure fragment has at least one destination.
        if (!f.destination_set()) {
            std::stringstream ss;
            ss << "Fragment " << f
               << " has no destinations set (not accessed"
                  " by any components).";
            throw std::runtime_error(ss.str());
        }

        // Make sure that each component in fragments destination collection has
        // this fragment as input.
        for (auto&& i = f.destinations_begin(); i < f.destinations_end(); ++i) {
            const Component& dst = **i;
            if (std::find(dst.inputs_begin(), dst.inputs_end(), &f) ==
                dst.inputs_end()) {
                std::stringstream ss;
                ss << "Component " << dst << " does not contain fragment " << f
                   << " as input, but is defined as fragment destination.";
                throw std::runtime_error(ss.str());
            }
        }
    }
}

void Configuration::results_source_and_destination_check() const
{
    // Make sure all results have source and destination set.
    for (const Result& r : results) {
        if (!r.source_set()) {
            std::stringstream ss;
            ss << "Result " << r << " has no source set.";
            throw std::runtime_error(ss.str());
        }
        if (!r.destination_set()) {
            std::stringstream ss;
            ss << "Result " << r << " has no destination set.";
            throw std::runtime_error(ss.str());
        }

        {
            const Component& dst = r.get_destination();
            if (std::find(dst.inputs_begin(), dst.inputs_end(), &r) ==
                dst.inputs_end()) {
                throw std::runtime_error("Component input out of sync with "
                                         "result destination.");
            }
        }
        {
            const Component& src = r.get_source();
            if (std::find(src.outputs_begin(), src.outputs_end(), &r) ==
                src.outputs_end()) {
                throw std::runtime_error("Component output out of sync with "
                                         "result source.");
            }
        }
    }
}

}
