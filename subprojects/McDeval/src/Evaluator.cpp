#include "McDeval/Evaluator.hpp"

#include <unordered_set>

namespace McDeval {

Evaluator::Evaluator(const Configuration& config) : config(config) {}

std::string Evaluator::type_to_string(Type evaluator_type)
{
    switch (evaluator_type) {
        case Type::Constraint: return "Constraint";
        case Type::SingleResource: return "Single Resource";
        case Type::Resident: return "Resident";
    }
    throw std::logic_error("Unhandled evaluator type!");
}

std::unique_ptr<Evaluator>
Evaluator::create_evaluator(Type evaluator_type, const Configuration& config)
{
    switch (evaluator_type) {
        case Type::Constraint:
            return std::make_unique<ConstraintEvaluator>(config);
        case Type::SingleResource:
            return std::make_unique<SingleResourceEvaluator>(config);
        case Type::Resident: return std::make_unique<ResidentEvaluator>(config);
    }
    throw std::logic_error("Unknown evaluator type!");
}

ConstraintEvaluator::ConstraintEvaluator(const Configuration& config,
                                         double punishment)
    : Evaluator(config), punishment(punishment)
{}

Risk ConstraintEvaluator::evaluate(const Solution& solution)
{
    Risk risk = 0.0;
    for (auto&& constraint : config.constraints) {
        ServerSet bs = constraint.get_breaking_servers(solution);

        // If breaking servers exists, constraint has been broken.
        if (!bs.empty()) {
            // Use trust of the most insecure breaking server to calculate risk.
            const Server& most_insecure = bs.get_most_insecure_server();
            risk += 1.0 / most_insecure.get_trust() * punishment;
        }
    }

    return risk;
}

ResidentEvaluator::ResidentEvaluator(const Configuration& config,
                                     double punishment)
    : Evaluator(config), punishment(punishment)
{}

Risk ResidentEvaluator::evaluate(const Solution& solution)
{
    Risk risk = 0.0;

    // Servers populated by fragments.
    std::unordered_set<const Server*> fragment_servers;
    for (const Fragment& f : config.fragments) {
        fragment_servers.emplace(&solution.get_server<Fragment>(f));
    }

    for (const Component& c : config.components) { // Component iteration.
        // Check if component co-resides with any fragment.
        if (fragment_servers.count(&solution.get_server<Component>(c)) == 1) {
            risk += punishment;
        }
    }

    return risk;
}

SingleResourceEvaluator::SingleResourceEvaluator(const Configuration& config)
    : Evaluator(config)
{}

Risk SingleResourceEvaluator::evaluate(const Solution& solution)
{
    Risk risk = 0.0;

    // For each fragment, calculate risk on each server that the fragment is
    // available and add those risks together.
    for (const Fragment& f : config.fragments) {
        // Get servers where fragment is available.
        ServerSet ss = f.server_reach(solution);

        // Calculate risk on each server and add it to total risk.
        for (const Server* s : ss) {
            risk += 1.0 / s->get_trust() * f.get_importance();
        }
    }

    // Do the same for results as was done with fragments.
    for (const Result& r : config.results) {
        // Get servers where result is available.
        ServerSet ss = r.server_reach(solution);

        // Calculate risk on each server and add it to total risk.
        for (const Server* s : ss) {
            risk += 1.0 / s->get_trust() * r.get_importance();
        }
    }

    return risk;
}

}
