#include "McDeval/Solution.hpp"

#include <algorithm>
#include <sstream>
#include <regex>

namespace McDeval {

namespace {
template<typename Out>
void split(const std::string& s, char delim, Out result)
{
    std::istringstream iss(s);
    std::string item;
    while (std::getline(iss, item, delim)) { *result++ = item; }
}

std::vector<std::string> split(const std::string& s, char delim);
}

template<>
void Solution::set_server<Fragment>(const Fragment& assignable,
                                    const Server& server)
{
    fragment_mapping[&assignable] = &server;
}

template<>
void Solution::set_server<Component>(const Component& assignable,
                                     const Server& server)
{
    component_mapping[&assignable] = &server;
}

template<>
const Server& Solution::get_server<Fragment>(const Fragment& assignable) const
{
    try {
        return *fragment_mapping.at(&assignable);
    } catch (const std::out_of_range&) {
        throw std::runtime_error("Fragment not assigned to the server.");
    }
}

template<>
const Server& Solution::get_server<Component>(const Component& assignable) const
{
    try {
        return *component_mapping.at(&assignable);
    } catch (const std::out_of_range&) {
        throw std::runtime_error("Component not assigned to the server.");
    }
}

Solution::Solution()
{
    constexpr std::size_t initial_size = 10;
    fragment_mapping.reserve(initial_size);
    component_mapping.reserve(initial_size);
}

Solution::Solution(const std::string& sol_as_string,
                   const Configuration& config)
{
    auto lines = split(sol_as_string, '\n');
    for (auto& line : lines) {
        auto tokens = split(line, ' ');

        // Trim all tokens (remove leading and trailing white space).
        for (auto& token : tokens) {
            token =
                std::regex_replace(token, std::regex("^ +| +$|( ) +"), "$1");
        }

        std::string server_name = tokens[0];
        server_name.pop_back(); // Remove colon.

        tokens.erase(tokens.begin()); // Remove server name from tokens vector.

        // Get server.
        const auto* server_p = config.get_server(server_name);
        if (!server_p) {
            std::stringstream err_msg;
            err_msg << "No server with name: " << server_name;
            throw std::runtime_error(err_msg.str());
        }

        for (const auto& token : tokens) {
            const auto* component_p = config.get_component(token);
            if (component_p) {
                set_server(*component_p, *server_p);
            } else { // Try fetching a fragment with that name.
                const auto* fragment_p = config.get_fragment(token);
                if (fragment_p) {
                    set_server(*fragment_p, *server_p);
                } else {
                    std::stringstream err_msg;
                    err_msg << "No fragment or component with name: " << token;
                    throw std::runtime_error(err_msg.str());
                }
            }
        }
    }
}

bool Solution::operator==(const Solution& other) const
{
    return fragment_mapping == other.fragment_mapping &&
           component_mapping == other.component_mapping;
}

std::ostream& operator<<(std::ostream& ostr, const Solution& solution)
{
    std::vector<const Server*> servers; // Servers sorted by ID (unique).
    for (auto&& i : solution.fragment_mapping) {
        const Server* s = i.second;

        // If the server has not yet been found, push it to the vector.
        auto it = std::find(servers.begin(), servers.end(), s);
        if (it == servers.end()) { servers.push_back(s); }
    }
    for (auto&& i : solution.component_mapping) {
        const Server* s = i.second;

        // If the server has not yet been found, push it to the vector.
        auto it = std::find(servers.begin(), servers.end(), s);
        if (it == servers.end()) { servers.push_back(s); }
    }

    std::sort(servers.begin(), servers.end()); // Sort servers by ID.

    // Fragments per server. Create empty vector for each server.
    std::vector<std::vector<const Fragment*>> fps(servers.size());

    // Fill fragments per server vector.
    for (auto&& i : solution.fragment_mapping) {
        const Fragment* f = i.first;
        const Server* s   = i.second;

        // Find position of the server in vector of sorted servers.
        auto it = std::find(servers.begin(), servers.end(), s);

        // Sanity check - should never be true.
        if (it == servers.end()) {
            throw std::runtime_error("Unknown fragment.");
        }

        // Find index of the server in vector of sorted servers.
        size_t index = std::distance(servers.begin(), it);

        fps[index].push_back(f);
    }

    // Components per server. Create empty vector for each server.
    std::vector<std::vector<const Component*>> cps(servers.size());

    // Fill components per server vector.
    for (auto&& i : solution.component_mapping) {
        const Component* c = i.first;
        const Server* s    = i.second;

        // Find position of the server in vector of sorted servers.
        auto it = std::find(servers.begin(), servers.end(), s);

        // Sanity check - should never be true.
        if (it == servers.end()) {
            throw std::runtime_error("Unknown fragment.");
        }

        // Find index of the server in vector of sorted servers.
        size_t index = std::distance(servers.begin(), it);

        cps[index].push_back(c);
    }

    // Print fragments and components for each server.
    for (size_t i = 0; i < servers.size(); i++) {
        ostr << *servers[i] << ": ";

        // Fragments for server at index 'i'.
        const std::vector<const Fragment*>& frgs = fps[i];
        for (auto&& i : frgs) { ostr << *i << ' '; }

        // Components for server at index 'i'.
        const std::vector<const Component*>& cmps = cps[i];
        for (auto&& i : cmps) { ostr << *i << ' '; }

        ostr << '\n';
    }

    return ostr;
}

namespace {

std::vector<std::string> split(const std::string& s, char delim)
{
    std::vector<std::string> elems;
    split(s, delim, std::back_inserter(elems));
    return elems;
}

}

}
