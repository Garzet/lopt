#ifndef CONTROLSPANEL_HPP
#define CONTROLSPANEL_HPP

#include <wx/wx.h>

class ControlsPanel : public wxPanel {
  private:
    wxButton* evaluate_button = nullptr;
    wxButton* clear_button    = nullptr;
    wxButton* quit_button     = nullptr;

  public:
    explicit ControlsPanel(wxWindow* parent);
};

#endif
