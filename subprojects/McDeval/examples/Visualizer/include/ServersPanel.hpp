#ifndef SERVERS_PANEL_HPP
#define SERVERS_PANEL_HPP

#include "CollectionPanel.hpp"
#include "DereferenceIterator.hpp"

/** Holds multiple server panels (depending on the configuration). */
class ServersPanel : public wxPanel {
  public:
    ServersPanel(wxWindow* parent);
    using RefIter = ReferenceIterator<wxWindowList::const_iterator>;

    RefIter begin() const;
    RefIter end() const;
};

/** Holds allocatable objects (components and fragments) that are assigned to
 * the server. This is a drop target (allocatable objects are dropped to this
 * panel). */
class ServerPanel : public wxPanel {
  public:
    const char index;

  private:
    /** Holds all allocatable objects. */
    CollectionPanel* inner;

  public:
    ServerPanel(ServersPanel* parent, char index);

    /** Adds a fragment and recalculates size. */
    void add_fragment(char index);

    /** Adds a component and recalculates size. */
    void add_component(char index);

    /** Returns names of allocatables assigned to this server. */
    std::vector<std::array<char, 2>> get_assigned_allocatable_names() const;
};

#endif
