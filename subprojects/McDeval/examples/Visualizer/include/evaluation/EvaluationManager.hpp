#ifndef EVALUATION_MANAGER_HPP
#define EVALUATION_MANAGER_HPP

#include "McDeval.hpp"

#include <filesystem>

namespace Evaluation {

void load_configuration(const std::filesystem::path& filename);

const McDeval::Configuration& get_configuration();

}

#endif
