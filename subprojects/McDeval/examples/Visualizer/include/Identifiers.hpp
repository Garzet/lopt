#ifndef IDENTIFIERS_HPP
#define IDENTIFIERS_HPP

#include <wx/wx.h>

namespace ID {

constexpr int evaluate_button = wxID_EXECUTE;
constexpr int quit_button     = wxID_EXIT;
constexpr int clear_button    = wxID_CLEAR;

}

#endif
