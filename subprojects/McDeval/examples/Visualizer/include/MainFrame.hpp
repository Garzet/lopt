#ifndef MAIN_FRAME_HPP
#define MAIN_FRAME_HPP

#include "ServersPanel.hpp"
#include "MathGLPanel.hpp"

#include <wx/wx.h>

class MainFrame : public wxFrame {
  private:
    ServersPanel* servers_panel       = nullptr;
    wxPanel* right_panel              = nullptr;
    CollectionPanel* fragments_panel  = nullptr;
    CollectionPanel* components_panel = nullptr;
    MathGLPanel* math_gl_panel        = nullptr;

  public:
    MainFrame();
    ~MainFrame() override = default;

  private:
    void on_quit_button_pressed(wxCommandEvent& event);
    void on_clear_button_pressed(wxCommandEvent& event);
    void on_evaluate_button_pressed(wxCommandEvent& event);

    wxDECLARE_EVENT_TABLE();
};

#endif
