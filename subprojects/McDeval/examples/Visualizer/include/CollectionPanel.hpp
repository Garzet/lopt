#ifndef COLLECTIONPANEL_HPP
#define COLLECTIONPANEL_HPP

#include "AllocatablePanel.hpp"

#include <wx/wx.h>

#include <vector>
#include <array>

/** A panel that holds multiple allocatable panels. */
class CollectionPanel : public wxPanel {
  public:
    explicit CollectionPanel(wxWindow* parent);

    /** Adds allocatable panel and recalculates size. */
    void add(AllocatablePanel* panel);

    /** Returns true if no allocatable panels has been added. */
    bool empty() const;

    /** Returns pointers to all allocatable panels in this collection panel. */
    std::vector<AllocatablePanel*> get_children() const;
};

#endif
