#include "AllocatablePanel.hpp"

#include <wx/dnd.h>

#include <sstream>
#include <array>

AllocatablePanel::AllocatablePanel(wxWindow* parent, char type, char index)
    : DrawPanel(parent), type(toupper(type)), index(index)
{
    // Make sure type is 'F' (fragment) or 'C' (component) (converted to upper).
    if (this->type != 'F' && this->type != 'C') {
        std::stringstream ss;
        ss << "Allocatable panel type must be 'F' (fragment) or 'C' "
           << "(component). Received character: " << this->type;
        throw std::logic_error(ss.str());
    }

    // Make sure index is digit (index of fragment or component).
    if (!isdigit(index)) {
        std::stringstream ss;
        ss << "Allocatable panel index must be a digit. Received character: "
           << index;
        throw std::logic_error(ss.str());
    }

    // Set the panel size.
    SetMinSize({54, 54});
    SetSize({54, 54});
}

void AllocatablePanel::on_mouse_down(wxMouseEvent& /*event*/)
{
    std::array<char, 2> data_string = {type, index};
    wxTextDataObject data(wxString(&data_string[0], 2));
    wxDropSource drag_source(this);
    drag_source.SetData(data);

    // TODO Try to get this drag and drop icon working.
    // drag_source.SetIcon(wxDragMove);

    wxDragResult result = drag_source.DoDragDrop(wxDragMove);

    // If fragment or component has been allocated to the server, remove it from
    // the collection panel since it will be drawn to the server panel.
    if (result == wxDragMove) {
        Hide();
        GetParent()->GetSizer()->RecalcSizes();

        // Remove the component or fragment.
        Destroy();
    }
}

void AllocatablePanel::on_mouse_enter(wxMouseEvent& /*event*/)
{
    SetCursor(wxCURSOR_HAND);
}

void AllocatablePanel::on_mouse_leave(wxMouseEvent& /*event*/)
{
    SetCursor(wxCURSOR_ARROW);
}

FragmentPanel::FragmentPanel(wxWindow* parent, char index)
    : AllocatablePanel(parent, 'F', index)
{}

void FragmentPanel::render(wxDC& dc)
{
    // Draw circle.
    dc.SetBrush(wxColor(150, 150, 150));   // Fill color.
    dc.SetPen(wxPen(wxColor(0, 0, 0), 5)); // Border color and thickness.
    dc.DrawRectangle({1, 1}, {52, 52});    // Rectangle point and dimensions.

    // Draw text inside circle.
    dc.SetTextForeground({0, 0, 0}); // Text color.
    dc.SetFont({18, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD});
    dc.DrawText('F', {17, 11});
    dc.SetFont({13, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD});
    dc.DrawText(index, {28, 24});
}

ComponentPanel::ComponentPanel(wxWindow* parent, char index)
    : AllocatablePanel(parent, 'C', index)
{}

void ComponentPanel::render(wxDC& dc)
{
    // Draw circle.
    dc.SetBrush(wxColor(150, 150, 150));   // Fill color.
    dc.SetPen(wxPen(wxColor(0, 0, 0), 4)); // Border color and thickness.
    dc.DrawCircle({27, 27}, 25);           // Circle center and radius.

    // Draw text inside circle.
    dc.SetTextForeground({0, 0, 0}); // Text color.
    dc.SetFont({18, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD});
    dc.DrawText('C', {14, 12});
    dc.SetFont({14, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD});
    dc.DrawText(index, {30, 21});
}

BEGIN_EVENT_TABLE(AllocatablePanel, DrawPanel) // clang-format off
    EVT_LEFT_DOWN(AllocatablePanel::on_mouse_down)
    EVT_ENTER_WINDOW(AllocatablePanel::on_mouse_enter)
    EVT_LEAVE_WINDOW(AllocatablePanel::on_mouse_leave)
END_EVENT_TABLE() // clang-format on
