#include "ServersPanel.hpp"

#include "AllocatablePanel.hpp"
#include "evaluation/EvaluationManager.hpp"

#include <wx/dnd.h>

#include <sstream>

namespace {
/** Implements function for receiving text via drag and drop. */
class TextDropTarget : public wxTextDropTarget {
  private:
    ServerPanel& server_panel;

  public:
    TextDropTarget(ServerPanel& server_panel);
    bool OnDropText(wxCoord x, wxCoord y, const wxString& data) override;

  private:
    static void validate_drop_text(const wxString& data);
};
}

ServersPanel::ServersPanel(wxWindow* parent) : wxPanel(parent)
{
    auto sizer = new wxBoxSizer(wxVERTICAL);
    SetSizer(sizer);
    for (auto& s : Evaluation::get_configuration().servers) {
        sizer->Add(new ServerPanel(this, s.id[1]),
                   0,
                   wxEXPAND | wxLEFT | wxRIGHT | wxTOP,
                   10);
    }
    sizer->wxSizer::AddSpacer(10);
}

ServersPanel::RefIter ServersPanel::begin() const
{
    return RefIter(m_children.begin());
}

ServersPanel::RefIter ServersPanel::end() const
{
    return RefIter(m_children.end());
}

ServerPanel::ServerPanel(ServersPanel* parent, char index)
    : wxPanel(parent), index(index), inner(new CollectionPanel(this))
{
    SetBackgroundColour(*wxBLACK); // Set the border color.
    inner->SetMinSize({70, 70});   // Set the panel size.
    inner->SetBackgroundColour({204, 204, 204});

    SetSizer(new wxBoxSizer(wxHORIZONTAL));
    constexpr static int padding = 5;
    GetSizer()->Add(inner, 1, wxEXPAND | wxALL, padding);

    {
        auto main_label = new wxStaticText(inner, wxID_ANY, 'S');
        main_label->SetFont(
            {18, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD});
        main_label->SetForegroundColour(*wxBLACK);
        inner->GetSizer()->Add(main_label, 0, wxEXPAND | wxLEFT, 5);
    }
    {
        auto index_label = new wxStaticText(inner, wxID_ANY, index);
        index_label->SetFont(
            {12, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD});
        index_label->SetForegroundColour(*wxBLACK);
        inner->GetSizer()->Add(index_label, 0, wxEXPAND | wxTOP, 10);
    }

    SetDropTarget(new TextDropTarget(*this)); // Accept text dropped.
}

void ServerPanel::add_fragment(char index)
{
    inner->add(new FragmentPanel(inner, index));
}

void ServerPanel::add_component(char index)
{
    inner->add(new ComponentPanel(inner, index));
}

std::vector<std::array<char, 2>>
ServerPanel::get_assigned_allocatable_names() const
{
    std::vector<std::array<char, 2>> names;
    auto children = inner->GetChildren();
    names.reserve(children.size());
    for (auto* child : children) {
        auto child_casted = dynamic_cast<AllocatablePanel*>(child);
        if (child_casted) {
            names.push_back({child_casted->type, child_casted->index});
        }
    }
    return names;
}

namespace {

TextDropTarget::TextDropTarget(ServerPanel& server_panel)
    : server_panel(server_panel)
{}

bool TextDropTarget::OnDropText(wxCoord /*x*/,
                                wxCoord /*y*/,
                                const wxString& data)
{
    // Make sure first character is 'F' or 'C' and second character is digit.
    validate_drop_text(data);

    // Extract first and second character.
    const char allocatable_identifier = std::toupper(data[0]);
    const char index                  = data[1];

    switch (allocatable_identifier) {
        case 'F': server_panel.add_fragment(index); break;
        case 'C': server_panel.add_component(index); break;
        default: throw std::logic_error("Uknown allocatable type.");
    }

    // std::cout << "Server S" << server_panel.index          // TODO: REMOVE
    //<< " received " << data << '.' << std::endl; // TODO: REMOVE

    return true; // Signal that data has been accepted.
}

void TextDropTarget::validate_drop_text(const wxString& data)
{
    // Make sure two characters are received.
    if (data.length() != 2) {
        std::stringstream ss;
        ss << "Drag and drop string length is expected to be two. "
              "Received string: '"
           << data << "'"
           << " (length: " << data.length() << ").";
        throw std::runtime_error(ss.str());
    }

    // Make sure first character is either 'F' or 'C'.
    const char first_char_upper = std::toupper(data[0]);
    if (first_char_upper != 'F' && first_char_upper != 'C') {
        std::stringstream ss;
        ss << "String does not start with 'F' or 'C'. Received string: "
           << data;
        throw std::logic_error(ss.str());
    }

    // Make sure second character is digit (index of fragment or component).
    const char index = data[1];
    if (!std::isdigit(index)) {
        std::stringstream ss;
        ss << "Fragment or component index not a digit. Received string: "
           << data;
        throw std::logic_error(ss.str());
    }
}

}

