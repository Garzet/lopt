#include "MathGLPanel.hpp"

#include <cstring>

MathGLPanel::MathGLPanel(wxWindow* parent) : wxPanel(parent)
{
    // Set border color.
    SetBackgroundColour(*wxBLACK);

    // Set the minimum size of the MathGLPanel.
    SetMinSize({500, 500});

    // Set font and mark size.
    graph.SetFontSize(2.8);
    graph.SetMarkSize(1.0);

    // Set axis range.
    constexpr static std::array<double, 2> x_range = {0, 250};
    constexpr static std::array<double, 2> y_range = {150, 1000};
    graph.SetRanges(y_range[0], y_range[1], x_range[0], x_range[1]);
    graph.SetOrigin(y_range[0], x_range[0]);

    // Do not use scientific notation on axis tics.
    graph.SetTickTempl('x', "%g");
    graph.SetTickTempl('y', "%g");
}

void MathGLPanel::on_paint_event(wxPaintEvent& /*event*/)
{
    int width, height;
    GetClientSize(&width, &height); // Get available size for the graph.

    constexpr static int min_dimensions = 100;
    if (width < min_dimensions || height < min_dimensions) {
        throw std::runtime_error("MathGL panel made too small.");
    }

    constexpr static int padding = 10;
    std::vector<unsigned char> graph_data =
        create_graph(width - padding, height - padding);
    const wxImage img(
        width - padding, height - padding, graph_data.data(), true);
    wxPaintDC dc(this);
    dc.DrawBitmap(wxBitmap(img), padding / 2, padding / 2);
}

std::vector<unsigned char> MathGLPanel::create_graph(unsigned width,
                                                     unsigned height)
{
    graph.SetSize(width, height, false); // Set graph size.
    graph.Clf(0.8, 0.8, 0.8);            // Clear the graph.

    graph.Axis();
    graph.Grid("xy", "{x838383};");
    graph.Label('x', "Single resource risk", 0);
    graph.Label('y', "Constraint risk", 0);

    // Convert plot data into MathGL data and plot it.
    if (!plot_data.empty()) {
        mglData x_points = plot_data.get_x_points();
        mglData y_points = plot_data.get_y_points();
        graph.Plot(x_points, y_points, " #{x000000}o");
    }

    // Copy the image data into local buffer to avoid casting (reinterpret cast
    // and const cast may cause undefined behavior).
    std::vector<unsigned char> graph_data(width * height * 3);
    std::memcpy(graph_data.data(), graph.GetRGB(), graph_data.size());
    return graph_data;
}

void MathGLPanel::clear()
{
    plot_data.clear();
    Refresh();
}

void MathGLPanel::add_point(std::array<double, 2> point)
{
    plot_data.add_point(point);
    Refresh();
}

wxBEGIN_EVENT_TABLE(MathGLPanel, wxPanel) // clang-format off
    EVT_PAINT(MathGLPanel::on_paint_event)
wxEND_EVENT_TABLE() // clang-format on
