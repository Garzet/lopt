#include "PlotData.hpp"

#include <stdexcept>

PlotData::PlotData(std::size_t reserve_size)
{
    x_points.reserve(reserve_size);
    y_points.reserve(reserve_size);
}

void PlotData::add_point(std::array<double, 2> point)
{
    x_points.push_back(point[0]);
    y_points.push_back(point[1]);
}

std::size_t PlotData::size() const
{
    if (x_points.size() != y_points.size()) {
        throw std::logic_error("X points and Y points are not the same size!");
    }
    return x_points.size();
}

bool PlotData::empty() const
{
    return size() == 0;
}

void PlotData::clear()
{
    x_points.clear();
    y_points.clear();
}

const std::vector<double>& PlotData::get_x_points() const
{
    return x_points;
}

const std::vector<double>& PlotData::get_y_points() const
{
    return y_points;
}

std::ostream& operator<<(std::ostream& s, const PlotData& pd)
{
    for (std::size_t i = 0; i < pd.size(); i++) {
        s << '{' << pd.x_points[i] << ',' << pd.y_points[i] << '}';
        if (i < pd.size() - 1) s << ", "; // Do not print comma on last element.
        if ((i + 1) % 10 == 0) s << '\n'; // Print new line every ten points.
    }
    return s;
}
