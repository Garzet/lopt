#include "CollectionPanel.hpp"

CollectionPanel::CollectionPanel(wxWindow* parent) : wxPanel(parent)
{
    SetSizer(new wxBoxSizer(wxHORIZONTAL));
}

void CollectionPanel::add(AllocatablePanel* panel)
{
    GetSizer()->Add(panel, 0, wxALIGN_CENTER_VERTICAL | wxLEFT, 10);
    GetSizer()->RecalcSizes();
}

bool CollectionPanel::empty() const
{
    return GetChildren().empty();
}

std::vector<AllocatablePanel*> CollectionPanel::get_children() const
{
    const auto children = GetChildren();
    std::vector<AllocatablePanel*> children_casted;
    children_casted.reserve(children.size());
    for (const auto child : children) {
        children_casted.push_back(dynamic_cast<AllocatablePanel*>(child));
    }
    return children_casted;
}
