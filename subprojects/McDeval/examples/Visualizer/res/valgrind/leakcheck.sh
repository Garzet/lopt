#!/bin/sh
EXECUTABLE_DIRECTORY=../..
MAKE_DIRECTORY="$EXECUTABLE_DIRECTORY"
EXECUTABLE=Visualizer
WXSUPPRESSIONS=wxwidgets.supp

CURRENT_DIRECTORY=$(pwd)
make debug -C "$MAKE_DIRECTORY" && cd "$EXECUTABLE_DIRECTORY" && \
    valgrind --leak-check=full \
             --suppressions="$CURRENT_DIRECTORY/$WXSUPPRESSIONS" \
             "./$EXECUTABLE"
cd "$CURRENT_DIRECTORY"
