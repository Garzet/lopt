#include <catch2/catch.hpp>

#include "McDeval.hpp"

#include "ConfigurationLoader.hpp"
#include "SolutionConstructor.hpp"

TEST_CASE("Breaking servers")
{
    // Load and validate the configuration.
    const McDeval::Configuration config =
        ConfigurationLoader::load("res/app_config_1.xml");
    config.validate();

    SECTION("First solution")
    {
        const McDeval::Solution solution =
            SolutionConstructor::construct_solution("S0: F0 C0\n"
                                                    "S1: F1\n"
                                                    "S2: F2\n"
                                                    "S3: C1 C2",
                                                    config);

        // Resource server reaches are as following:
        //      F0:  S0
        //      F1:  S1 S3
        //      F2:  S2 S3
        //      R12: S3

        SECTION("First constraint")
        {
            McDeval::ServerSet expected_breaking_servers({});
            McDeval::ServerSet calculated_beaking_servers =
                config.constraints[0].get_breaking_servers(solution);
            REQUIRE(calculated_beaking_servers == expected_breaking_servers);
        }

        SECTION("Second constraint")
        {
            McDeval::ServerSet expected_breaking_servers({&config.servers[3]});
            McDeval::ServerSet calculated_beaking_servers =
                config.constraints[1].get_breaking_servers(solution);
            REQUIRE(calculated_beaking_servers == expected_breaking_servers);
        }

        SECTION("Third constraint")
        {
            McDeval::ServerSet expected_breaking_servers({&config.servers[3]});
            McDeval::ServerSet calculated_beaking_servers =
                config.constraints[2].get_breaking_servers(solution);
            REQUIRE(calculated_beaking_servers == expected_breaking_servers);
        }

        SECTION("Fourth constraint")
        {
            McDeval::ServerSet expected_breaking_servers({});
            McDeval::ServerSet calculated_beaking_servers =
                config.constraints[3].get_breaking_servers(solution);
            REQUIRE(calculated_beaking_servers == expected_breaking_servers);
        }
    }

    SECTION("Second solution")
    {
        const McDeval::Solution solution =
            SolutionConstructor::construct_solution("S2: C0 C1 C2\n"
                                                    "S3: F0 F1 F2",
                                                    config);

        // Resource server reaches are as following:
        //      F0:  S2 S3
        //      F1:  S2 S3
        //      F2:  S2 S3
        //      R12: S2

        SECTION("First constraint")
        {
            McDeval::ServerSet expected_breaking_servers(
                {&config.servers[2], &config.servers[3]});
            McDeval::ServerSet calculated_beaking_servers =
                config.constraints[0].get_breaking_servers(solution);
            REQUIRE(calculated_beaking_servers == expected_breaking_servers);
        }

        SECTION("Second constraint")
        {
            McDeval::ServerSet expected_breaking_servers(
                {&config.servers[2], &config.servers[3]});
            McDeval::ServerSet calculated_beaking_servers =
                config.constraints[1].get_breaking_servers(solution);
            REQUIRE(calculated_beaking_servers == expected_breaking_servers);
        }

        SECTION("Third constraint")
        {
            McDeval::ServerSet expected_breaking_servers({&config.servers[2]});
            McDeval::ServerSet calculated_beaking_servers =
                config.constraints[2].get_breaking_servers(solution);
            REQUIRE(calculated_beaking_servers == expected_breaking_servers);
        }

        SECTION("Fourth constraint")
        {
            McDeval::ServerSet expected_breaking_servers({&config.servers[2]});
            McDeval::ServerSet calculated_beaking_servers =
                config.constraints[3].get_breaking_servers(solution);
            REQUIRE(calculated_beaking_servers == expected_breaking_servers);
        }
    }
}
