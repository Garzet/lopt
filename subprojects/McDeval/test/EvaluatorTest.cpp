#include <catch2/catch.hpp>

#include "McDeval.hpp"

#include "ConfigurationLoader.hpp"
#include "SolutionConstructor.hpp"

TEST_CASE("Evaluation")
{
    // Load and validate the configuration.
    const McDeval::Configuration config =
        ConfigurationLoader::load("res/app_config_2.xml");
    config.validate();

    // Create evaluators.
    McDeval::ResidentEvaluator resident_evaluator(config, 100);
    McDeval::SingleResourceEvaluator single_resource_evaluator(config);
    McDeval::ConstraintEvaluator constraint_evaluator(config, 100);

    SECTION("First solution")
    {
        const McDeval::Solution solution =
            SolutionConstructor::construct_solution("S2: C2 C0\n"
                                                    "S3: F0 F1 F2 C1",
                                                    config);

        REQUIRE(resident_evaluator.evaluate(solution).get_value() ==
                Approx(100.00).margin(0.01));
        REQUIRE(single_resource_evaluator.evaluate(solution).get_value() ==
                Approx(223.33).margin(0.01));
        REQUIRE(constraint_evaluator.evaluate(solution).get_value() ==
                Approx(33.33).margin(0.01));
    }

    SECTION("Second solution")
    {
        const McDeval::Solution solution =
            SolutionConstructor::construct_solution("S2: C0 C1 C2\n"
                                                    "S3: F0 F1 F2",
                                                    config);

        REQUIRE(resident_evaluator.evaluate(solution).get_value() ==
                Approx(0.00).margin(0.01));
        REQUIRE(single_resource_evaluator.evaluate(solution).get_value() ==
                Approx(380.00).margin(0.01));
        REQUIRE(constraint_evaluator.evaluate(solution).get_value() ==
                Approx(33.33).margin(0.01));
    }

    SECTION("Third solution")
    {
        const McDeval::Solution solution =
            SolutionConstructor::construct_solution("S2: C1 C2\n"
                                                    "S3: C0 F0 F1 F2",
                                                    config);

        REQUIRE(resident_evaluator.evaluate(solution).get_value() ==
                Approx(100.00).margin(0.01));
        REQUIRE(single_resource_evaluator.evaluate(solution).get_value() ==
                Approx(359.17).margin(0.01));
        REQUIRE(constraint_evaluator.evaluate(solution).get_value() ==
                Approx(0.00).margin(0.01));
    }
}
