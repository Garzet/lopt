#include "SolutionConstructor.hpp"

#include <sstream>
#include <regex>

namespace {

/** Gets component by name from the provided configuration object. Returns
 * nullptr if no component with provided name exists in the provided
 * configuration object. */
const McDeval::Component* get_component(const std::string& name,
                                        const McDeval::Configuration& config);

/** Gets fragment by name from the provided configuration object. Returns
 * nullptr if no fragment with provided name exists in the provided
 * configuration object. */
const McDeval::Fragment* get_fragment(const std::string& name,
                                      const McDeval::Configuration& config);

/** Gets server by name from the provided configuration object. Returns nullptr
 * if no server with provided name exists in the provided configuration object.
 */
const McDeval::Server* get_server(const std::string& name,
                                  const McDeval::Configuration& config);

template<typename Out>
void split(const std::string& s, char delim, Out result)
{
    std::istringstream iss(s);
    std::string item;
    while (std::getline(iss, item, delim)) { *result++ = item; }
}

std::vector<std::string> split(const std::string& s, char delim);

}

McDeval::Solution
SolutionConstructor::construct_solution(const std::string& sol_as_string,
                                        const McDeval::Configuration& config)
{
    McDeval::Solution solution;
    auto lines = split(sol_as_string, '\n');
    for (auto& line : lines) {
        auto tokens = split(line, ' ');

        // Trim all tokens (remove leading and trailing white space).
        for (auto& token : tokens) {
            token =
                std::regex_replace(token, std::regex("^ +| +$|( ) +"), "$1");
        }

        std::string server_name = tokens[0];
        server_name.pop_back(); // Remove colon.

        tokens.erase(tokens.begin()); // Remove server name from tokens vector.

        // Get server.
        auto server_p = get_server(server_name, config);
        if (!server_p) {
            std::stringstream err_msg;
            err_msg << "No server with name: " << server_name;
            throw std::runtime_error(err_msg.str());
        }

        for (const auto& token : tokens) {
            auto component_p = get_component(token, config);
            if (component_p) {
                solution.set_server(*component_p, *server_p);
            } else { // Try fetching a fragment with that name.
                auto fragment_p = get_fragment(token, config);
                if (fragment_p) {
                    solution.set_server(*fragment_p, *server_p);
                } else {
                    std::stringstream err_msg;
                    err_msg << "No fragment or component with name: " << token;
                    throw std::runtime_error(err_msg.str());
                }
            }
        }
    }
    return solution;
}

namespace {

const McDeval::Component* get_component(const std::string& name,
                                        const McDeval::Configuration& config)
{
    auto res_iter =
        std::find(config.components.begin(), config.components.end(), name);
    if (res_iter == config.components.end()) return nullptr;
    return &*res_iter;
}

const McDeval::Fragment* get_fragment(const std::string& name,
                                      const McDeval::Configuration& config)
{
    auto res_iter =
        std::find(config.fragments.begin(), config.fragments.end(), name);
    if (res_iter == config.fragments.end()) return nullptr;
    return &*res_iter;
}

const McDeval::Server* get_server(const std::string& name,
                                  const McDeval::Configuration& config)
{
    auto res_iter =
        std::find(config.servers.begin(), config.servers.end(), name);
    if (res_iter == config.servers.end()) return nullptr;
    return &*res_iter;
}

std::vector<std::string> split(const std::string& s, char delim)
{
    std::vector<std::string> elems;
    split(s, delim, std::back_inserter(elems));
    return elems;
}

}
