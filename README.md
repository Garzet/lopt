# Layout Optimizer (Lopt)

Distributed application layout optimizer for security in a multi-cloud
environment.

## Getting started

These instructions will get you a copy of the project up and running on your 
local machine.

You will need:
* C++17 compiler
* [Meson](https://mesonbuild.com/)

1. Clone the repository: `git clone https://gitlab.com/fer_group/lopt Lopt`.
2. Position inside the directory: `cd Lopt`.
3. Generate build files: `meson build`.
4. Position inside the build directory: `cd build`.
5. Build the project: `ninja`.
6. Run the program with desired configuration files:
   `./Lopt <app_config.xml> <alg_config.xml>`.

## Adjusting parameters

Lopt parameters are split into two files:
    1. Application configuration - describes the multi-cloud application, data
                                   flow through the application and available
                                   cloud providers
    2. Algorithm configuration - configures the optimization algorithm used to
                                 optimize the multi-cloud application deployment 

Examples of these configuration files can be seen in `res` directory.

## Authors

* **Rudolf Lovrenčić** (rudolf.lovrencic@fer.hr)

## License

This project is licensed under the MIT License - see the
[LICENSE](LICENSE) file for details
